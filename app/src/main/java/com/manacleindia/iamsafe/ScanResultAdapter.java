/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.manacleindia.iamsafe;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;

import com.manacleindia.iamsafe.ui.MainActivity;
import com.manacleindia.iamsafe.ui.ScannerFragment;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Holds and displays {@link ScanResult}s, used by {@link ScannerFragment}.
 */
public class ScanResultAdapter extends BaseAdapter {

    private ArrayList<ScanResult> mArrayList;

    private Context mContext;

    private LayoutInflater mInflater;

    ScanResultAdapter(Context context, LayoutInflater inflater) {
        super();
        mContext = context;
        mInflater = inflater;
        mArrayList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mArrayList.get(position).getDevice().getAddress().hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        // Reuse an old view if we can, otherwise create a new one.
        if (view == null) {
            view = mInflater.inflate(R.layout.listitem_scanresult, null);
        }

        TextView deviceNameView = view.findViewById(R.id.device_name);
        TextView deviceAddressView = view.findViewById(R.id.device_address);
        TextView lastSeenView = view.findViewById(R.id.last_seen);
        TextView ressi = view.findViewById(R.id.ressi);
        TextView distanceTextView = view.findViewById(R.id.distance);
        TextView safeTextView = view.findViewById(R.id.safeMessage);
        LinearLayout mRowLayout = view.findViewById(R.id.rowLayout);

        ScanResult scanResult = mArrayList.get(position);

        String name = scanResult.getDevice().getName();
        if (name == null) {
            name = mContext.getResources().getString(R.string.no_name);
        }
        deviceNameView.setText(name);
        deviceAddressView.setText(scanResult.getDevice().getAddress());
        lastSeenView.setText(getTimeSinceString(mContext, scanResult.getTimestampNanos()));
        ressi.setText("RSSI: " + scanResult.getRssi());

        /*
        *
        * Formula
Distance
Measured Power
RSSI
N (Constant depends on the Environmental factor. Range 2-4)

 
Distance = 10 ^ ((Measured Power – RSSI)/(10 * N))
￼
 
Measured Power is also known as the 1 Meter RSSI.
 
Let’s check some examples :
Measured Power = -69 (for kontakt BLE beacons)
RSSI = -60, -69, -80
N = 2
1. Distance for RSSI -60 = 10 ^ ((-69 – (-60))/(10 * 2))
= 0.35 meter
        * */


        DecimalFormat form = new DecimalFormat("0.00");
        form.setRoundingMode(RoundingMode.CEILING);
        double dis3 = getDistance(scanResult.getRssi(), -69);

        String ff = form.format(dis3);

        if (dis3 >= 3) {
            Log.v("TAG", "GREEN");
            safeTextView.setText("You are Safe");
            mRowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.md_green_500));
        } else if (dis3 <= 1) {
            Log.v("TAG", "RED");
            safeTextView.setText("You are in the Risk! You must have to follow Social Distancing!!");
            mRowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.md_red_500));
        } else if (dis3 > 1 && dis3 < 3) {
            Log.v("TAG", "ORANGE");
            safeTextView.setText("You are not Safe! You should follow Social Distancing!!");
            mRowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.md_orange_500));
        }

        distanceTextView.setText(String.format("Distance: %s Meter", ff));

        return view;
    }

    private double getDistance(int rssi, int txPower) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         *
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */

        return Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
    }

    private static double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
    }

    /**
     * Search the adapter for an existing device address and return it, otherwise return -1.
     */
    private int getPosition(String address) {
        int position = -1;
        for (int i = 0; i < mArrayList.size(); i++) {
            if (mArrayList.get(i).getDevice().getAddress().equals(address)) {
                position = i;
                break;
            }
        }
        return position;
    }


    /**
     * Add a ScanResult item to the adapter if a result from that device isn't already present.
     * Otherwise updates the existing position with the new ScanResult.
     */
    void add(ScanResult scanResult) {

        int existingPosition = getPosition(scanResult.getDevice().getAddress());

        if (existingPosition >= 0) {
            // Device is already in list, update its record.
            mArrayList.set(existingPosition, scanResult);
        } else {
            // Add new Device's ScanResult to list.
            mArrayList.add(scanResult);
        }
    }

    /**
     * Clear out the adapter.
     */
    public void clear() {
        mArrayList.clear();
    }

    /**
     * Takes in a number of nanoseconds and returns a human-readable string giving a vague
     * description of how long ago that was.
     */
    private static String getTimeSinceString(Context context, long timeNanoseconds) {
        String lastSeenText = context.getResources().getString(R.string.last_seen) + " ";

        long timeSince = SystemClock.elapsedRealtimeNanos() - timeNanoseconds;
        long secondsSince = TimeUnit.SECONDS.convert(timeSince, TimeUnit.NANOSECONDS);

        if (secondsSince < 5) {
            lastSeenText += context.getResources().getString(R.string.just_now);
        } else if (secondsSince < 60) {
            lastSeenText += secondsSince + " " + context.getResources()
                    .getString(R.string.seconds_ago);
        } else {
            long minutesSince = TimeUnit.MINUTES.convert(secondsSince, TimeUnit.SECONDS);
            if (minutesSince < 60) {
                if (minutesSince == 1) {
                    lastSeenText += minutesSince + " " + context.getResources()
                            .getString(R.string.minute_ago);
                } else {
                    lastSeenText += minutesSince + " " + context.getResources()
                            .getString(R.string.minutes_ago);
                }
            } else {
                long hoursSince = TimeUnit.HOURS.convert(minutesSince, TimeUnit.MINUTES);
                if (hoursSince == 1) {
                    lastSeenText += hoursSince + " " + context.getResources()
                            .getString(R.string.hour_ago);
                } else {
                    lastSeenText += hoursSince + " " + context.getResources()
                            .getString(R.string.hours_ago);
                }
            }
        }

        return lastSeenText;
    }

    private void sendNotification(String messageBody, String title, int zoneType) {
        try {
            Random r = new Random();
            final int NOTIFICATION_ID = 101;//r.nextInt(10000);
            Intent intent = new Intent(mContext, MainActivity.class);

            intent.putExtra("isComingFromNotification", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            String channelId = mContext.getString(R.string.default_notification_channel_id);
            //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            /*RemoteViews contentView = null;
            if (zoneType == 1) {
                contentView = new RemoteViews(mContext.getPackageName(), R.layout.custom__red_notification_layout);
            } else if (zoneType == 2) {
                contentView = new RemoteViews(mContext.getPackageName(), R.layout.custom_notification_layout);
            }

            Objects.requireNonNull(contentView).setTextViewText(R.id.title, title);
            contentView.setTextViewText(R.id.content, messageBody);*/

            Uri sound = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.siren_noise);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    //.setContent(contentView)
                    .setVibrate(new long[]{1000, 1000})
                    .setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setSound(sound)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    AudioAttributes.Builder attrs = new AudioAttributes.Builder();
                    attrs.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
                    attrs.setUsage(AudioAttributes.USAGE_NOTIFICATION);
                    //Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel notificationChannel = new NotificationChannel(channelId, "AmSafe", importance);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.BLUE);
                    notificationChannel.enableVibration(true);
                    notificationChannel.setSound(sound, attrs.build());
                    //notificationChannel.setShowBadge(true);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
                    notificationBuilder.setChannelId(channelId);
                    Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            Objects.requireNonNull(notificationManager).notify(NOTIFICATION_ID, notificationBuilder.build());
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
