package com.manacleindia.iamsafe.db;

class DBConstant {
    //Database Name
    private static final String DATABASE_NAME = "aTrack19";
    private static final int DATABASE_VERSION = 1;
    //Table Name
    private static final String TRACK_TABLE = "track";
    //Tree Table Columns Name
    private static final String TRACK_ID = "trackId";
    private static final String TRACK_TIME = "trackTime";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String BLE_DEVICE_NAME = "bleDeviceName";
    private static final String DEVICE_NAME = "deviceName";
    private static final String DEVICE_TOKEN = "deviceToken";
    private static final String TRACK_DATE = "trackDate";
    private static final String BLE_MAC_ADDRESS = "bleMacAddress";
    private static final String IP_ADDRESS = "ipAddress";
    private static final String IMEI = "imei";
    private static final String OS_VERSION = "osVersion";
    private static final String USER_ID = "userID";

    static String getDatabaseName() {
        return DATABASE_NAME;
    }

    static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    static String getTrackTable() {
        return TRACK_TABLE;
    }

    static String getTrackId() {
        return TRACK_ID;
    }

    static String getTrackTime() {
        return TRACK_TIME;
    }

    static String getLATITUDE() {
        return LATITUDE;
    }

    static String getLONGITUDE() {
        return LONGITUDE;
    }

    static String getDeviceName() {
        return DEVICE_NAME;
    }

    static String getBleDeviceName() {
        return BLE_DEVICE_NAME;
    }

    static String getDeviceToken() {
        return DEVICE_TOKEN;
    }

    static String getTrackDate() {
        return TRACK_DATE;
    }

    static String getBleMacAddress() {
        return BLE_MAC_ADDRESS;
    }

    static String getIpAddress() {
        return IP_ADDRESS;
    }

    static String getIMEI() {
        return IMEI;
    }

    static String getOsVersion() {
        return OS_VERSION;
    }

    static String getUserId() {
        return USER_ID;
    }
}
