package com.manacleindia.iamsafe.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class MVCDBModel {
    //Table Creation
    private static final String TREE_TABLE_CREATE_QUERY = "create table "
            + DBConstant.getTrackTable()
            + " ( " + DBConstant.getTrackId() + " TEXT PRIMARY KEY not null,"
            + DBConstant.getTrackDate() + " TEXT,"
            + DBConstant.getTrackTime() + " TEXT,"
            + DBConstant.getLATITUDE() + " INTEGER,"
            + DBConstant.getLONGITUDE() + " TEXT,"
            + DBConstant.getBleDeviceName() + " TEXT,"
            + DBConstant.getBleMacAddress() + " TEXT,"
            + DBConstant.getDeviceName() + " TEXT,"
            + DBConstant.getDeviceToken() + " TEXT,"
            + DBConstant.getIMEI() + " TEXT,"
            + DBConstant.getIpAddress() + " TEXT,"
            + DBConstant.getUserId() + " TEXT,"
            + DBConstant.getOsVersion() + " TEXT)";

    private SQLiteDatabase database;

    private final SQLiteOpenHelper helper;

    MVCDBModel(final Context ctx) {
        this.helper = new SQLiteOpenHelper(ctx, DBConstant.getDatabaseName(), null, DBConstant.getDatabaseVersion()) {
            @Override
            public void onCreate(final SQLiteDatabase db) {
                db.execSQL(TREE_TABLE_CREATE_QUERY);
            }

            @Override
            public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + TREE_TABLE_CREATE_QUERY);
                this.onCreate(db);
            }
        };
        this.database = this.helper.getWritableDatabase();
    }

    long addLoop(ContentValues data) {
        this.database = this.helper.getWritableDatabase();
        return this.database.insert(DBConstant.getTrackTable(), null, data);
    }

    Cursor getAllLoopList() {
        this.database = this.helper.getReadableDatabase();
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime(), DBConstant.getLATITUDE(), DBConstant.getLONGITUDE(), DBConstant.getBleDeviceName(), DBConstant.getDeviceName(), DBConstant.getDeviceToken(), DBConstant.getTrackDate(), DBConstant.getIpAddress(), DBConstant.getIMEI(), DBConstant.getOsVersion(), DBConstant.getUserId()};
        return this.database.query(DBConstant.getTrackTable(), columns, null, null, null, null, DBConstant.getTrackId() + " DESC");
    }

    Cursor getAllTrackDateList() {
        this.database = this.helper.getReadableDatabase();
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime(), DBConstant.getTrackDate()};
        return this.database.query(DBConstant.getTrackTable(), columns, null, null, DBConstant.getTrackDate(), null, DBConstant.getTrackId() + " DESC");
    }

    Cursor getTrackFromDate(String date) {
        this.database = this.helper.getReadableDatabase();
        String[] selectionArgs = new String[]{date};
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime()};
        return this.database.query(DBConstant.getTrackTable(), columns, DBConstant.getTrackDate() + "=?", selectionArgs, null, null, null);
    }

    Cursor getTimeOnTrack(String date) {
        this.database = this.helper.getReadableDatabase();
        String[] selectionArgs = new String[]{date};
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime()};
        return this.database.query(DBConstant.getTrackTable(), columns, DBConstant.getTrackDate() + "=?", selectionArgs, DBConstant.getTrackTime(), null, null);
    }

    Cursor getTrackFromId(String trackId) {
        this.database = this.helper.getReadableDatabase();
        String[] selectionArgs = new String[]{trackId};
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime(), DBConstant.getLATITUDE(), DBConstant.getLONGITUDE(), DBConstant.getBleDeviceName(), DBConstant.getDeviceName(), DBConstant.getDeviceToken(), DBConstant.getTrackDate(), DBConstant.getIpAddress(), DBConstant.getIMEI(), DBConstant.getOsVersion(), DBConstant.getUserId()};
        return this.database.query(DBConstant.getTrackTable(), columns, DBConstant.getTrackId() + "=?", selectionArgs, null, null, null);
    }

    Cursor isTrackAlreadyExist(final String trackId) {
        this.database = this.helper.getWritableDatabase();
        String[] selectionArgs = new String[]{trackId};
        return this.database.rawQuery("select * from " + DBConstant.getTrackTable() + " where " + DBConstant.getTrackId() + "=?", selectionArgs);
    }

    Cursor getTotalCount() {
        this.database = this.helper.getWritableDatabase();
        return this.database.rawQuery("select * from " + DBConstant.getTrackTable(), null);
    }

    Cursor getTotalTimeCount() {
        this.database = this.helper.getReadableDatabase();
        String[] columns = new String[]{DBConstant.getTrackId(), DBConstant.getTrackTime()};
        return this.database.query(DBConstant.getTrackTable(), columns, null, null, DBConstant.getTrackTime(), null, null);
    }

    Cursor isAnyTrackExist() {
        this.database = this.helper.getWritableDatabase();
        return this.database.rawQuery("select * from " + DBConstant.getTrackTable(), null);
    }

    int deleteTrack(String trackId) {
        this.database = this.helper.getWritableDatabase();
        String[] selectionArgs = new String[]{trackId};
        return this.database.delete(DBConstant.getTrackTable(), DBConstant.getTrackId() + "=?", selectionArgs);
    }
}
