package com.manacleindia.iamsafe.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.manacleindia.iamsafe.ui.detail.TrackerModel;

import java.util.ArrayList;

public class MVCDBController {
    private MVCDBModel model;

    public MVCDBController(final Context context) {
        model = new MVCDBModel(context);
    }

    public long addTrackData(TrackerModel trackerModel) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstant.getTrackId(), trackerModel.getTrackId());
        contentValues.put(DBConstant.getTrackTime(), trackerModel.getTrackTime());
        contentValues.put(DBConstant.getLATITUDE(), trackerModel.getLatitude());
        contentValues.put(DBConstant.getLONGITUDE(), trackerModel.getLongitude());
        contentValues.put(DBConstant.getDeviceName(), trackerModel.getDeviceName());
        contentValues.put(DBConstant.getBleDeviceName(), trackerModel.getBleDeviceName());
        contentValues.put(DBConstant.getDeviceToken(), trackerModel.getDeviceToken());
        contentValues.put(DBConstant.getTrackDate(), trackerModel.getTrackDate());
        contentValues.put(DBConstant.getBleMacAddress(), trackerModel.getBleMacAddress());
        contentValues.put(DBConstant.getIpAddress(), trackerModel.getIpAddress());
        contentValues.put(DBConstant.getIMEI(), trackerModel.getImeiNumber());
        contentValues.put(DBConstant.getOsVersion(), trackerModel.getOsVersion());

        return model.addLoop(contentValues);
    }

    public ArrayList<TrackerModel> getAllTrackList() {
        ArrayList<TrackerModel> arrayList = new ArrayList<>();
        Cursor cursor = model.getAllLoopList();
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    TrackerModel musicLoop = new TrackerModel();

                    musicLoop.setTrackId(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackId())));
                    musicLoop.setTrackTime(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackTime())));
                    musicLoop.setTrackDate(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackDate())));

                    musicLoop.setBleDeviceName(cursor.getString(cursor.getColumnIndex(DBConstant.getBleDeviceName())));
                    musicLoop.setBleMacAddress(cursor.getString(cursor.getColumnIndex(DBConstant.getBleMacAddress())));
                    musicLoop.setDeviceName(cursor.getString(cursor.getColumnIndex(DBConstant.getDeviceName())));

                    musicLoop.setDeviceToken(cursor.getString(cursor.getColumnIndex(DBConstant.getDeviceToken())));
                    musicLoop.setImeiNumber(cursor.getString(cursor.getColumnIndex(DBConstant.getIMEI())));
                    musicLoop.setIpAddress(cursor.getString(cursor.getColumnIndex(DBConstant.getIpAddress())));

                    musicLoop.setLatitude(cursor.getString(cursor.getColumnIndex(DBConstant.getLATITUDE())));
                    musicLoop.setLongitude(cursor.getString(cursor.getColumnIndex(DBConstant.getLONGITUDE())));
                    musicLoop.setOsVersion(cursor.getString(cursor.getColumnIndex(DBConstant.getOsVersion())));

                    arrayList.add(musicLoop);
                }
            } finally {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList<TrackerModel> getAllTrackDateList() {
        ArrayList<TrackerModel> arrayList = new ArrayList<>();
        Cursor cursor = model.getAllTrackDateList();
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    TrackerModel musicLoop = new TrackerModel();

                    musicLoop.setTrackId(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackId())));
                    musicLoop.setTrackTime(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackTime())));
                    musicLoop.setTrackDate(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackDate())));

                    arrayList.add(musicLoop);
                }
            } finally {
                cursor.close();
            }
        }
        return arrayList;
    }

    public ArrayList<TrackerModel> getAllTrackTimeList(String date) {
        ArrayList<TrackerModel> arrayList = new ArrayList<>();
        Cursor cursor = model.getTimeOnTrack(date);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row
                    TrackerModel musicLoop = new TrackerModel();
                    musicLoop.setTrackId(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackId())));
                    musicLoop.setTrackTime(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackTime())));
                    arrayList.add(musicLoop);
                }
            } finally {
                cursor.close();
            }
        }
        return arrayList;
    }

    public TrackerModel getTrackFromId(String trackId) {
        TrackerModel musicLoop = new TrackerModel();
        Cursor cursor = model.getTrackFromId(trackId);
        if (cursor != null) {
            try {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    // do something with data in current row

                    musicLoop.setTrackId(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackId())));
                    musicLoop.setTrackTime(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackTime())));
                    musicLoop.setTrackDate(cursor.getString(cursor.getColumnIndex(DBConstant.getTrackDate())));

                    musicLoop.setBleDeviceName(cursor.getString(cursor.getColumnIndex(DBConstant.getBleDeviceName())));
                    musicLoop.setBleMacAddress(cursor.getString(cursor.getColumnIndex(DBConstant.getBleMacAddress())));
                    musicLoop.setDeviceName(cursor.getString(cursor.getColumnIndex(DBConstant.getDeviceName())));

                    musicLoop.setDeviceToken(cursor.getString(cursor.getColumnIndex(DBConstant.getDeviceToken())));
                    musicLoop.setImeiNumber(cursor.getString(cursor.getColumnIndex(DBConstant.getIMEI())));
                    musicLoop.setIpAddress(cursor.getString(cursor.getColumnIndex(DBConstant.getIpAddress())));

                    musicLoop.setLatitude(cursor.getString(cursor.getColumnIndex(DBConstant.getLATITUDE())));
                    musicLoop.setLongitude(cursor.getString(cursor.getColumnIndex(DBConstant.getLONGITUDE())));
                    musicLoop.setOsVersion(cursor.getString(cursor.getColumnIndex(DBConstant.getOsVersion())));
                }
            } finally {
                cursor.close();
            }
        }
        return musicLoop;
    }

    public boolean isTrackAlreadyExist(final String trackId) {
        try (Cursor cursor = model.isTrackAlreadyExist(trackId)) {
            return cursor.getCount() > 0;
        }
    }

    public int getTotalCount() {
        try (Cursor cursor = model.getTotalTimeCount()) {
            return cursor.getCount();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isAnyTrackExist() {
        try (Cursor cursor = model.isAnyTrackExist()) {
            return cursor.getCount() > 0;
        }
    }

}
