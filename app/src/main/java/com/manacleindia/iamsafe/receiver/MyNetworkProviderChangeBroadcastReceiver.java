package com.manacleindia.iamsafe.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;


public class MyNetworkProviderChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.requireNonNull(intent.getAction()).matches("android.net.conn.CONNECTIVITY_CHANGE")
                || intent.getAction().matches("android.net.wifi.WIFI_STATE_CHANGED")) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
            if (netInfo != null && netInfo.isConnected()) {
                saveLog("Internet On: ", "Enable internet " + intent.getAction());
            } else {
                saveLog("Internet Off: ", "Disable internet " + intent.getAction());
            }
        } else {
            saveLog("BootCompleteReceiver Invoked", "BootCompleteReceiver " + intent.getAction());
        }

        //Toast.makeText(context, "MyNetworkProviderChangeBroadcastReceiver", Toast.LENGTH_SHORT).show();

        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent);
        asyncTask.execute();

    }

    private static class Task extends AsyncTask<String, Integer, String> {

        private final PendingResult pendingResult;
        private final Intent intent;

        private Task(PendingResult pendingResult, Intent intent) {
            this.pendingResult = pendingResult;
            this.intent = intent;
        }

        @Override
        protected String doInBackground(String... strings) {
            String log = "Action: " + intent.getAction() + "\n" +
                    "URI: " + intent.toUri(Intent.URI_INTENT_SCHEME) + "\n";
            return log;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Must call finish() so the BroadcastReceiver can be recycled.
            saveLog("Broadcast Task: ", "Task " + s);
            pendingResult.finish();
        }
    }
}
