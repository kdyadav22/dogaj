package com.manacleindia.iamsafe.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.manacleindia.iamsafe.location.StartUpService;

public class MyLocationProvideChangeBroadcastReceiver extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        StartUpService.enqueueWork(context, intent);
    }
}
