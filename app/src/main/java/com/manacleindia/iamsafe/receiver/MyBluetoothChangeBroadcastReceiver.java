package com.manacleindia.iamsafe.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;


import com.manacleindia.iamsafe.services.MyService;
import com.manacleindia.iamsafe.ui.dashboard.AdvertiserService;

import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;


public class MyBluetoothChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle extras = intent.getExtras();
        final int bluetoothState = Objects.requireNonNull(extras).getInt("android.bluetooth.adapter.extra.STATE");
        switch (bluetoothState) {
            case BluetoothAdapter.STATE_OFF:
                // Bluetooth OFF
                /*Intent serviceIntent = new Intent(AppController.getInstance(), MyService.class);
                serviceIntent.putExtra("inputState", "STATE_OFF");
                serviceIntent.putExtra("IsComingFromBLEChange", true);*/
                MyService.enqueueWork(context, intent);
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
                //Toast.makeText(context, "STATE_TURNING_OFF", Toast.LENGTH_SHORT).show();

                // Turning OFF
                break;
            case BluetoothAdapter.STATE_ON:
                // Bluetooth ON
                if (!AdvertiserService.running) {
                    saveLog("My Bluetooth Receiver: ", "Register BLE & Advertisement When Bluetooth ON");
                    startAdvertisement(context);
                } else {
                   /* Intent serviceIntentOn = new Intent(AppController.getInstance(), MyService.class);
                    serviceIntentOn.putExtra("inputState", "STATE_ON");
                    serviceIntentOn.putExtra("IsComingFromBLEChange", true);*/
                    MyService.enqueueWork(context, intent);
                }
                break;
            case BluetoothAdapter.STATE_TURNING_ON:
                // Turning ON
                //Toast.makeText(context, "STATE_TURNING_ON", Toast.LENGTH_SHORT).show();

                break;
        }


        /*final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent);
        asyncTask.execute();*/

    }

    private static class Task extends AsyncTask<String, Integer, String> {

        private final PendingResult pendingResult;
        private final Intent intent;

        private Task(PendingResult pendingResult, Intent intent) {
            this.pendingResult = pendingResult;
            this.intent = intent;
        }

        @Override
        protected String doInBackground(String... strings) {
            String log = "Action: " + intent.getAction() + "\n" +
                    "URI: " + intent.toUri(Intent.URI_INTENT_SCHEME) + "\n";
            return log;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Must call finish() so the BroadcastReceiver can be recycled.
            saveLog("Broadcast Task: ", "Task " + s);
            pendingResult.finish();
        }
    }

    private void startAdvertisement(Context context) {
        try {
           /* Intent serviceIntent = new Intent(AppController.getInstance(), AdvertiserService.class);
            serviceIntent.putExtra("MyServiceStarted", true);
            ContextCompat.startForegroundService(context, serviceIntent);*/

            startAdvertising(context);
        } catch (Exception e) {
            saveLog("startAdvertising onHandleWork Exception: ", "startAdvertising onHandleWork Exception " + e.getMessage());
            e.getMessage();
        }
    }


    /**
     * Starts BLE Advertising by starting {@code AdvertiserService}.
     */
    private void startAdvertising(Context context) {
        Objects.requireNonNull(context).startService(getServiceIntent(context));
    }

    /**
     * Returns Intent addressed to the {@code AdvertiserService} class.
     */
    private static Intent getServiceIntent(Context c) {
        return new Intent(c, AdvertiserService.class);
    }
}
