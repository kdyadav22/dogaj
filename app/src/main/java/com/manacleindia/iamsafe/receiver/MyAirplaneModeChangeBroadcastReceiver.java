package com.manacleindia.iamsafe.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;


public class MyAirplaneModeChangeBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
         if (Objects.requireNonNull(intent.getAction()).matches("android.intent.action.AIRPLANE_MODE")) {
            if (intent.getBooleanExtra("state", false)) {
                saveLog("Airplane Mode On: ", "Enabled airplane mode " + intent.getAction());
            } else {
                saveLog("Airplane Mode Off: ", "Disabled airplane mode " + intent.getAction());
            }
        } else {
            saveLog("BootCompleteReceiver Invoked", "BootCompleteReceiver " + intent.getAction());
        }

        //Toast.makeText(context, "MyAirplaneModeChangeBroadcastReceiver", Toast.LENGTH_SHORT).show();

        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent);
        asyncTask.execute();

    }

    private static class Task extends AsyncTask<String, Integer, String> {

        private final PendingResult pendingResult;
        private final Intent intent;

        private Task(PendingResult pendingResult, Intent intent) {
            this.pendingResult = pendingResult;
            this.intent = intent;
        }

        @Override
        protected String doInBackground(String... strings) {
            String log = "Action: " + intent.getAction() + "\n" +
                    "URI: " + intent.toUri(Intent.URI_INTENT_SCHEME) + "\n";
            return log;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Must call finish() so the BroadcastReceiver can be recycled.
            saveLog("Broadcast Task: ", "Task " + s);
            pendingResult.finish();
        }
    }
}
