package com.manacleindia.iamsafe.receiver;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.manacleindia.iamsafe.services.MyBLEService;

import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;

public class AlarmBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        BluetoothAdapter mBluetoothAdapter = ((BluetoothManager) Objects.requireNonNull(context.getSystemService(Context.BLUETOOTH_SERVICE))).getAdapter();
        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {
            // Is Bluetooth turned on?
            if (mBluetoothAdapter.isEnabled()) {
                saveLog("AlarmBroadcastReceiver ", "Alarm Received On BLE ON");
                Log.v("TAG",  "Alarm Received On BLE ON");
            } else {
                saveLog("AlarmBroadcastReceiver ", "Alarm Received On BLE OFF");
                Log.v("TAG",  "Alarm Received On BLE OFF");
            }
        }

        //Toast.makeText(context, "Alarm Received", Toast.LENGTH_SHORT).show();

        Log.v("TAG",  "Alarm Received");
        MyBLEService.enqueueWork(context, intent);
    }
}