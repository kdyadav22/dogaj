package com.manacleindia.iamsafe.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.manacleindia.iamsafe.location.StartUpService;
import com.manacleindia.iamsafe.services.MyService;

import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;


public class MyBootCompletedBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.requireNonNull(intent.getAction()).equals(Intent.ACTION_LOCKED_BOOT_COMPLETED) || intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
                || intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON") || intent.getAction().equals(Intent.ACTION_REBOOT)
                || intent.getAction().equals(Intent.ACTION_USER_PRESENT) || intent.getAction().equals("com.htc.intent.action.QUICKBOOT_POWERON")) {
            saveLog("BootCompleted Receiver ", "Action " + intent.getAction());
            MyService.enqueueWork(context, intent);
            StartUpService.enqueueWork(context, intent);
        } else if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            saveLog("Provider Changed: ", "Change Internet Provider " + intent.getAction());
        } else if (intent.getAction().matches("android.net.conn.CONNECTIVITY_CHANGE")
                || intent.getAction().matches("android.net.wifi.WIFI_STATE_CHANGED")) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;
            if (netInfo != null && netInfo.isConnected()) {
                saveLog("Internet On: ", "Enable internet " + intent.getAction());
            } else {
                saveLog("Internet Off: ", "Disable internet " + intent.getAction());
            }
        } else if (intent.getAction().matches("ACTION_BACKGROUND_DATA_SETTING_CHANGED")) {
            saveLog("Action Background Setting Changed: ", "ACTION_BACKGROUND " + intent.getAction());
        } else if (intent.getAction().matches("android.intent.action.AIRPLANE_MODE")) {
            if (intent.getBooleanExtra("state", false)) {
                saveLog("Airplane Mode On: ", "Enabled airplane mode " + intent.getAction());
            } else {
                saveLog("Airplane Mode Off: ", "Disabled airplane mode " + intent.getAction());
            }
        } else if (intent.getAction().matches("android.bluetooth.adapter.action.STATE_CHANGED")) {
            saveLog("Bluetooth Provider Changed: ", "Bluetooth " + intent.getAction());
        } else if (intent.getAction().equals(Intent.ACTION_SHUTDOWN) || intent.getAction().equals("android.intent.action.QUICKBOOT_POWEROFF")) {
            saveLog("Switch Off: ", "Switch Off device " + intent.getAction());
        } else {
            saveLog("BootCompleted Receiver Invoked", "Action " + intent.getAction());
        }
    }
}
