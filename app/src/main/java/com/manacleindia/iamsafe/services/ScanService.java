package com.manacleindia.iamsafe.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.common.AppController;
import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.common.SaveLog;
import com.manacleindia.iamsafe.common.Utility;
import com.manacleindia.iamsafe.db.MVCDBController;
import com.manacleindia.iamsafe.ui.MainActivity;
import com.manacleindia.iamsafe.ui.dashboard.ForegroundService;
import com.manacleindia.iamsafe.ui.detail.TrackerModel;
import com.pixplicity.easyprefs.library.Prefs;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Manages BLE Advertising independent of the main app.
 * If the app goes off screen (or gets killed completely) advertising can continue because this
 * Service is maintaining the necessary Callback in memory.
 */
public class ScanService extends Service {
    private BluetoothLeScanner mBluetoothLeScanner;

    /**
     * A global variable to let AdvertiserFragment check if the Service is running without needing
     * to start or bind to it.
     * This is the best practice method as defined here:
     * https://groups.google.com/forum/#!topic/android-developers/jEvXMWgbgzE
     */
    public static boolean running = false;
    /**
     * Stops scanning after 5 seconds.
     */
    private static final long SCAN_PERIOD = 1000 * 60 * 60 * 24 * 365;
    private ScanCallback mScanCallback;
    private Handler mHandler;
    @SuppressLint("SimpleDateFormat")
    private
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private MVCDBController mvcdbController;
    private static final String TAG = ScanService.class.getSimpleName();

    @Override
    public void onCreate() {
        mHandler = new Handler();
        mvcdbController = new MVCDBController(getApplicationContext());
        BluetoothAdapter mBluetoothAdapter = ((BluetoothManager) Objects.requireNonNull(getSystemService(Context.BLUETOOTH_SERVICE))).getAdapter();
        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {
            mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }

        startScanning();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Required for extending service, but this will be a Started Service only, so no need for
     * binding.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * Start scanning for BLE Advertisements (& set it up to stop after a set period of time).
     */
    private void startScanning() {
        if (mScanCallback == null) {
            // Will stop the scanning after a set time.
            mHandler.postDelayed(this::stopScanning, SCAN_PERIOD);

            // Kick off a new scan.
            mScanCallback = new SampleScanCallback();
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
        }
    }

    /**
     * Stop scanning for BLE Advertisements.
     */
    private void stopScanning() {
        running = false;
        // Stop the scan, wipe the callback.
        mBluetoothLeScanner.stopScan(mScanCallback);
        mScanCallback = null;
    }

    /**
     * Return a List of {@link ScanFilter} objects to filter by Service UUID.
     */
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        // Comment out the below line to see all BLE devices around you
        builder.setServiceUuid(Constants.Service_UUID);
        scanFilters.add(builder.build());

        return scanFilters;
    }

    /**
     * Return a {@link ScanSettings} object set to use low power (to preserve battery life).
     */
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        return builder.build();
    }

    /**
     * Custom ScanCallback object - adds to adapter on success, displays error on failure.
     */
    public class SampleScanCallback extends ScanCallback {

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);

            /*for (ScanResult result : results) {
                mAdapter.add(result);
            }
            mAdapter.notifyDataSetChanged();*/
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            if (result != null) {
               /* mAdapter.add(result);
                mAdapter.notifyDataSetChanged();*/
                running = true;
                DecimalFormat form = new DecimalFormat("0.00");
                form.setRoundingMode(RoundingMode.CEILING);
                double dis3 = getDistance(result.getRssi(), -69);
                if (dis3 <= 1) {
                    SaveLog.saveLog("Scanning Result", "Red");
                    TrackerModel trackerModel = new TrackerModel();

                    trackerModel.setTrackId(Utility.unique_ProfileId());

                    String name = result.getDevice().getName();
                    if (name == null) {
                        try {
                            name = getApplicationContext().getResources().getString(R.string.no_name);
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

                    trackerModel.setBleDeviceName(name);
                    trackerModel.setBleMacAddress(result.getDevice().getAddress());
                    trackerModel.setDeviceName("Android");
                    trackerModel.setDeviceToken(String.valueOf(System.currentTimeMillis()));
                    trackerModel.setImeiNumber("");

                    try {
                        trackerModel.setIpAddress("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    trackerModel.setLatitude("" + Prefs.getDouble(PreferenceConstants.LOCATION_LAT, 0.0));
                    trackerModel.setLongitude("" + Prefs.getDouble(PreferenceConstants.LOCATION_LNG, 0.0));
                    trackerModel.setOsVersion(Utility.getOsVersion());


                    trackerModel.setTrackDate(Utility.formattedDateString(dateFormat, new Date()));

                    @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("hh:mm a");
                    //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    String text = formatter.format(new Date(System.currentTimeMillis()));

                    trackerModel.setTrackTime(text);

                    if (mvcdbController.addTrackData(trackerModel) > 0) {
                        sendNotification(getApplicationContext(), "You are in the Risk! You must have to follow Social Distancing Guide Lines", "Risk Zone", 1);
                    }
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            running = false;
        }
    }

    private void sendNotification(Context context, String messageBody, String title, int zoneType) {
        try {
            Random r = new Random();
            final int NOTIFICATION_ID = 101;//r.nextInt(10000);

            if (context == null) {
                startService(messageBody, title);
                return;
            }

            Intent intent = new Intent(context, MainActivity.class);

            intent.putExtra("isComingFromNotification", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            String channelId = Objects.requireNonNull(context).getString(R.string.default_notification_channel_id);
            //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Uri defaultSoundUri = Uri.parse("android.resource://" + AppController.getInstance().getPackageName() + "/" + R.raw.siren_noise);

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(messageBody);

            notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            if (Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true)) {
                notificationBuilder.setVibrate(new long[]{1000, 1000});
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

            }

            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setWhen(System.currentTimeMillis());

            if (Prefs.getBoolean(PreferenceConstants.IS_SOUND_CHECKED, true)) {
                notificationBuilder.setSound(defaultSoundUri);
            }

            notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    AudioAttributes.Builder attrs = new AudioAttributes.Builder();
                    attrs.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
                    attrs.setUsage(AudioAttributes.USAGE_MEDIA);
                    //Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel notificationChannel = new NotificationChannel(channelId, "AmSafe", importance);
                    notificationChannel.enableLights(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        notificationChannel.setAllowBubbles(true);
                    }
                    notificationChannel.setBypassDnd(true);
                    notificationChannel.setImportance(importance);
                    notificationChannel.setLightColor(Color.BLUE);

                    if (Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true)) {
                        notificationChannel.enableVibration(true);
                        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    }

                    if (Prefs.getBoolean(PreferenceConstants.IS_SOUND_CHECKED, true)) {
                        notificationChannel.setSound(defaultSoundUri, attrs.build());
                    }

                    notificationChannel.setShowBadge(true);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
                    notificationBuilder.setChannelId(channelId);
                    Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            Objects.requireNonNull(notificationManager).notify(NOTIFICATION_ID, notificationBuilder.build());
            startForeground(NOTIFICATION_ID, notificationBuilder.build());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private double getDistance(int rssi, int txPower) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         *
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */

        return Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
    }

    private void startService(String messageBody, String title) {
        Intent serviceIntent = new Intent(AppController.getInstance(), ForegroundService.class);
        serviceIntent.putExtra("inputTitle", title);
        serviceIntent.putExtra("inputMessage", messageBody);
        ContextCompat.startForegroundService(Objects.requireNonNull(AppController.getInstance()), serviceIntent);
    }
}
