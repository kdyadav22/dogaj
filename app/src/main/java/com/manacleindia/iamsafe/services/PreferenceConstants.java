package com.manacleindia.iamsafe.services;

public class PreferenceConstants {
    public static String LOCATION_LAT;
    public static String LOCATION_LNG;
    public static String IS_VIBRATION_CHECKED;
    public static String IS_SOUND_CHECKED;
    public static String LOCATION_UPDATE;
}
