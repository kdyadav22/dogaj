package com.manacleindia.iamsafe.services;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.receiver.AlarmBroadcastReceiver;
import com.manacleindia.iamsafe.receiver.MyBluetoothChangeBroadcastReceiver;
import com.manacleindia.iamsafe.receiver.MyBootCompletedBroadcastReceiver;
import com.manacleindia.iamsafe.ui.MainActivity;
import com.manacleindia.iamsafe.ui.dashboard.AdvertiserService;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Calendar;
import java.util.Objects;
import java.util.Random;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;

/*
 * This Service calling from BootCompletion, Bluetooth Change & AlarmBroadCast
 *
 * When BLE ON,,Then Advertise & SCAN
 * Otherwise Show Notification
 *
 * */

public class MyService extends JobIntentService {
    public static final int JOB_ID = 0x01;
    MyBluetoothChangeBroadcastReceiver myBluetoothChangeBroadcastReceiver;
    MyBootCompletedBroadcastReceiver myBootCompletedBroadcastReceiver;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, MyService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        BluetoothAdapter mBluetoothAdapter = ((BluetoothManager) Objects.requireNonNull(getSystemService(Context.BLUETOOTH_SERVICE))).getAdapter();
        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {
            // Is Bluetooth turned on?
            if (mBluetoothAdapter.isEnabled()) {
                if (AdvertiserService.running) {
                    sendNotification("This device is discoverable to others nearby.", "AmSafe");
                } else {
                    startAdvertisement();
                }

                try {
                    if (!ScanService.running)
                        startScanning(getApplicationContext());
                } catch (Exception e) {
                    e.getMessage();
                }

            } else {
                //Display Notification But No start Advertisement/Scanning
                try {
                    sendNotification("Bluetooth must be ON all times to give alert message.", "AmSafe");
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            try {
                myBluetoothChangeBroadcastReceiver = new MyBluetoothChangeBroadcastReceiver();
                registerReceiver(myBluetoothChangeBroadcastReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

                myBootCompletedBroadcastReceiver = new MyBootCompletedBroadcastReceiver();
                registerReceiver(myBootCompletedBroadcastReceiver, new IntentFilter(Constants.BOOT_COMPLETED_STATE_CHANGED));
            } catch (Exception e) {
                e.getMessage();
            }
        }

        try {
            startAlarmBroadcastReceiver(getApplicationContext(), 5000);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void startAlarmBroadcastReceiver(Context context, long delay) {
        try {
            Intent _intent = new Intent(context, AlarmBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, _intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            // Set the alarm to start at 8:30 a.m.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 3);
            calendar.set(Calendar.MINUTE, 12);

            // Remove any previous pending intent.
            Objects.requireNonNull(alarmManager).cancel(pendingIntent);

            // setRepeating() lets you specify a precise custom interval--in this case,
            // 20 minutes.
            Objects.requireNonNull(alarmManager).setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 60000, pendingIntent);

            // Hopefully your alarm will have a lower frequency than this!
            //alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,60000, pendingIntent);

            //alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
            //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
        } catch (Exception e) {
            e.getMessage();
        }


    }

    private void startAdvertisement() {
        try {
            /*Intent serviceIntent = new Intent(AppController.getInstance(), AdvertiserService.class);
            serviceIntent.putExtra("MyServiceStarted", true);
            ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);*/
            startAdvertising(getApplicationContext());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void sendNotification(String messageBody, String title) {
        try {
            Random r = new Random();
            final int NOTIFICATION_ID = 1001;//r.nextInt(10000);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);

            intent.putExtra("isComingFromNotification", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            String channelId = getApplicationContext().getString(R.string.bg_notification_channel_id);
            //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri defaultSoundUri = Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.siren_noise);

            Bitmap bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setLargeIcon(bitmap);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(messageBody);
            //.setVibrate(new long[]{1000, 1000})
            notificationBuilder.setAutoCancel(false);
            notificationBuilder.setWhen(System.currentTimeMillis());
            notificationBuilder.setSound(defaultSoundUri);
            notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
            notificationBuilder.setPriority(Notification.PRIORITY_LOW);
            notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS);

            if (Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true)) {
                notificationBuilder.setVibrate(new long[]{1000, 1000});
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

            }

            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    AudioAttributes.Builder attrs = new AudioAttributes.Builder();
                    attrs.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
                    attrs.setUsage(AudioAttributes.USAGE_MEDIA);
                    Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    int importance = NotificationManager.IMPORTANCE_LOW;
                    NotificationChannel notificationChannel = new NotificationChannel(channelId, "AmSafe", importance);
                    notificationChannel.enableLights(false);
                    notificationChannel.setLightColor(Color.BLUE);
                    //notificationChannel.enableVibration(true);
                    //notificationChannel.setSound(defaultSoundUri, attrs.build());
                    notificationChannel.setShowBadge(false);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
                    notificationBuilder.setChannelId(channelId);
                    Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            Objects.requireNonNull(notificationManager).notify(NOTIFICATION_ID, notificationBuilder.build());

            startForeground(NOTIFICATION_ID, notificationBuilder.build());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(myBluetoothChangeBroadcastReceiver);
            unregisterReceiver(myBootCompletedBroadcastReceiver);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Starts BLE Advertising by starting {@code AdvertiserService}.
     */
    private void startAdvertising(Context context) {
        Objects.requireNonNull(context).startService(getServiceIntent(context));
    }

    /**
     * Returns Intent addressed to the {@code AdvertiserService} class.
     */
    private static Intent getServiceIntent(Context c) {
        return new Intent(c, AdvertiserService.class);
    }

    /**
     * Starts BLE Scanning by starting {@code ScanService}.
     */
    private void startScanning(Context context) {
        Objects.requireNonNull(context).startService(getScanningServiceIntent(context));
    }

    /**
     * Returns Intent addressed to the {@code ScanService} class.
     */
    private static Intent getScanningServiceIntent(Context c) {
        return new Intent(c, ScanService.class);
    }


}