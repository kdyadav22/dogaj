package com.manacleindia.iamsafe.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.receiver.AlarmBroadcastReceiver;
import com.manacleindia.iamsafe.receiver.MyBluetoothChangeBroadcastReceiver;
import com.manacleindia.iamsafe.receiver.MyBootCompletedBroadcastReceiver;
import com.manacleindia.iamsafe.ui.dashboard.AdvertiserService;

import java.util.Calendar;
import java.util.Objects;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;

public class MyBLEService extends JobIntentService {

    public static final int JOB_ID = 0x02;
    private static final int FOREGROUND_NOTIFICATION_ID = 1;
    MyBluetoothChangeBroadcastReceiver myBluetoothChangeBroadcastReceiver;
    MyBootCompletedBroadcastReceiver myBootCompletedBroadcastReceiver;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, MyBLEService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // your code
        BluetoothAdapter mBluetoothAdapter = ((BluetoothManager) Objects.requireNonNull(getSystemService(Context.BLUETOOTH_SERVICE))).getAdapter();
        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {
            // Is Bluetooth turned on?
            if (mBluetoothAdapter.isEnabled()) {
                if (!AdvertiserService.running) {
                    try {
                        startAdvertising(getApplicationContext());
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }

                try {
                    if (!ScanService.running)
                        startScanning(getApplicationContext());
                } catch (Exception e) {
                    e.getMessage();
                }

            }

            try {
                myBluetoothChangeBroadcastReceiver = new MyBluetoothChangeBroadcastReceiver();
                registerReceiver(myBluetoothChangeBroadcastReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

                myBootCompletedBroadcastReceiver = new MyBootCompletedBroadcastReceiver();
                registerReceiver(myBootCompletedBroadcastReceiver, new IntentFilter(Constants.BOOT_COMPLETED_STATE_CHANGED));
            } catch (Exception e) {
                e.getMessage();
            }
        }

        try {
            startAlarmBroadcastReceiver(getApplicationContext(), 5000);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public static void startAlarmBroadcastReceiver(Context context, long delay) {

        try {
            Intent _intent = new Intent(context, AlarmBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, _intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            // Set the alarm to start at 8:30 a.m.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 3);
            calendar.set(Calendar.MINUTE, 12);

            // Remove any previous pending intent.
            Objects.requireNonNull(alarmManager).cancel(pendingIntent);

            // setRepeating() lets you specify a precise custom interval--in this case,
            // 20 minutes.
            Objects.requireNonNull(alarmManager).setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), 60000, pendingIntent);

            // Hopefully your alarm will have a lower frequency than this!
            //alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,60000, pendingIntent);

            //alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
            //alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, pendingIntent);
        } catch (Exception e) {
            e.getMessage();
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(myBluetoothChangeBroadcastReceiver);
            unregisterReceiver(myBootCompletedBroadcastReceiver);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * Starts BLE Advertising by starting {@code AdvertiserService}.
     */
    private void startAdvertising(Context context) {
        Objects.requireNonNull(context).startService(getServiceIntent(context));
    }

    /**
     * Returns Intent addressed to the {@code AdvertiserService} class.
     */
    private static Intent getServiceIntent(Context c) {
        return new Intent(c, AdvertiserService.class);
    }

    /**
     * Starts BLE Scanning by starting {@code ScanService}.
     */
    private void startScanning(Context context) {
        Objects.requireNonNull(context).startService(getScanningServiceIntent(context));
    }

    /**
     * Returns Intent addressed to the {@code ScanService} class.
     */
    private static Intent getScanningServiceIntent(Context c) {
        return new Intent(c, ScanService.class);
    }
}