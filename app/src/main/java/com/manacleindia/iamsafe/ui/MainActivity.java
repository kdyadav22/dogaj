/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.manacleindia.iamsafe.ui;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.common.FragmentHelper;
import com.manacleindia.iamsafe.ui.dashboard.CDashboard;

import java.util.List;
import java.util.Objects;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Setup display fragments and ensure the device supports Bluetooth.
 */
public class MainActivity extends FragmentActivity implements EasyPermissions.PermissionCallbacks {
    private static final int STORAGE_REQUEST_CODE = 0x3;

    public ImageView refresh;
    public ImageView back;
    private BluetoothAdapter mBluetoothAdapter;
    private static MainActivity mInstance;

    public static synchronized MainActivity getInstance() {
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.activity_main_title);
        refresh = findViewById(R.id.refresh);
        back = findViewById(R.id.back);

        if (mInstance == null) {
            mInstance = this;
        }

        String[] permissionParam;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionParam = new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION};
        } else {
            permissionParam = new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!EasyPermissions.hasPermissions(MainActivity.this, permissionParam)) {
                EasyPermissions.requestPermissions(MainActivity.this, getResources().getString(R.string.rationale_storage), STORAGE_REQUEST_CODE, permissionParam);
            } else {
                setupBLE();
            }
        } else {
            setupBLE();
        }

        back.setOnClickListener(v -> {
            back.setVisibility(View.GONE);
            refresh.setVisibility(View.VISIBLE);
            onBackPressed();
        });

        try {
            refresh.setOnClickListener(v -> {
               /* startAdvertising();
                startScanning();*/
                loadSettingsFragment();
                Toast.makeText(this, "Hiii", Toast.LENGTH_SHORT).show();
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mInstance == null) {
            mInstance = this;
        }
    }

    private void setupBLE() {
        //if (savedInstanceState == null) {

        mBluetoothAdapter = ((BluetoothManager) Objects.requireNonNull(getSystemService(Context.BLUETOOTH_SERVICE))).getAdapter();

        // Is Bluetooth supported on this device?
        if (mBluetoothAdapter != null) {

            // Is Bluetooth turned on?
            if (mBluetoothAdapter.isEnabled()) {
                // Are Bluetooth Advertisements supported on this device?
                if (mBluetoothAdapter.isMultipleAdvertisementSupported()) {
                    // Everything is supported and enabled, load the fragments.
                    setupFragments();
                } else {
                    // Bluetooth Advertisements are not supported.
                    //showErrorText(R.string.bt_ads_not_supported);
                    setupFragments();
                }
            } else {

                // Prompt user to turn on Bluetooth (logic continues in onActivityResult()).
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
            }
        } else {

            // Bluetooth is not supported.
            showErrorText(R.string.bt_not_supported);
        }

        //}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {

                // Bluetooth is now Enabled, are Bluetooth Advertisements supported on
                // this device?
                if (mBluetoothAdapter.isMultipleAdvertisementSupported()) {

                    // Everything is supported and enabled, load the fragments.
                    setupFragments();

                } else {
                    setupFragments();
                    // Bluetooth Advertisements are not supported.
                    //showErrorText(R.string.bt_ads_not_supported);
                }
            } else {

                // User declined to enable Bluetooth, exit the app.
                Toast.makeText(this, R.string.bt_not_enabled_leaving,
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public int getContainerId() {
        return R.id.scanner_fragment_container;
    }

    private void setupFragments(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        ScannerFragment scannerFragment = new ScannerFragment();
        // Fragments can't access system services directly, so pass it the BluetoothAdapter
        scannerFragment.setBluetoothAdapter(mBluetoothAdapter);
        transaction.replace(R.id.scanner_fragment_container, scannerFragment);

        /*AdvertiserFragment advertiserFragment = new AdvertiserFragment();
        transaction.replace(R.id.advertiser_fragment_container, advertiserFragment);*/

        transaction.commit();
    }

    private void showErrorText(int messageId) {
        TextView view = findViewById(R.id.error_textview);
        view.setText(getString(messageId));
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        setupBLE();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, MainActivity.this);
    }


    private void loadSettingsFragment() {
        try {
            FragmentHelper.replaceFragmentAddToBackstack(Objects.requireNonNull(this).getSupportFragmentManager(), MainActivity.getInstance().getContainerId(),
                    SettingsFragment.getInstance(), SettingsFragment.class.getName());
        } catch (Exception e) {
            e.getMessage();
        }
    }

}