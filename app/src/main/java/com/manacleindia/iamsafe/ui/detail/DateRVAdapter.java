package com.manacleindia.iamsafe.ui.detail;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.databinding.ItemScanresultBinding;

import java.util.List;

public class DateRVAdapter extends RecyclerView.Adapter<DateRVAdapter.MusicViewHandler> {
    private Activity activity;
    private List<TrackerModel> loopList;
    private SelectionCallback musicSelectionCallback;

    public DateRVAdapter(Activity activity, List<TrackerModel> loopList, SelectionCallback musicSelectionCallback) {
        this.activity = activity;
        this.loopList = loopList;
        this.musicSelectionCallback = musicSelectionCallback;
    }

    @NonNull
    @Override
    public MusicViewHandler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemScanresultBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_scanresult, parent, false);
        return new MusicViewHandler(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final MusicViewHandler holder, final int position) {
        holder.setIsRecyclable(true);
        final TrackerModel data = loopList.get(position);
        holder.itemRowBinding.safeDate.setText(data.getTrackDate());
        holder.itemRowBinding.rowLayout.setOnClickListener(v -> musicSelectionCallback.onSelect(position, data));
    }

    @Override
    public int getItemCount() {
        return loopList.size();
    }

    public void removeAt(int pos) {
        loopList.remove(pos);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, loopList.size());
        notifyDataSetChanged();
    }

    static class MusicViewHandler extends RecyclerView.ViewHolder {
        ItemScanresultBinding itemRowBinding;

        MusicViewHandler(ItemScanresultBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public interface SelectionCallback {
        void onSelect(int pos, TrackerModel data);
    }

}
