package com.manacleindia.iamsafe.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.location.EnableGPSDialog;
import com.manacleindia.iamsafe.location.GetLocation;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class Splash extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final int STORAGE_REQUEST_CODE = 0x3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (new EnableGPSDialog().checkGPS(Splash.this) && Prefs.getBoolean(PreferenceConstants.LOCATION_UPDATE, false)) {
            startLocation();
        }

        String[] permissionParam;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionParam = new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        } else {
            permissionParam = new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!EasyPermissions.hasPermissions(Splash.this, permissionParam)) {
                EasyPermissions.requestPermissions(Splash.this, getResources().getString(R.string.rationale_storage), STORAGE_REQUEST_CODE, permissionParam);
            } else {
                if (new EnableGPSDialog().checkGPS(Splash.this))
                    startLocation();
                else new EnableGPSDialog().enableLoc(Splash.this);

                new Handler().postDelayed(() -> startActivity(new Intent(Splash.this, MainActivity.class)), 3000);

            }
        } else {
            if (new EnableGPSDialog().checkGPS(Splash.this))
                startLocation();
            else new EnableGPSDialog().enableLoc(Splash.this);

            new Handler().postDelayed(() -> startActivity(new Intent(Splash.this, MainActivity.class)), 3000);

        }

    }

    private void startLocation() {
        try {
            GetLocation getLocation = new GetLocation(this);
            getLocation.createLocationRequest();
            getLocation.getContinueLocationUpdates();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (new EnableGPSDialog().checkGPS(Splash.this))
            startLocation();
        else new EnableGPSDialog().enableLoc(Splash.this);

        new Intent(Splash.this, MainActivity.class);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}
