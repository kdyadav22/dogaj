package com.manacleindia.iamsafe.ui.detail;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.databinding.FragmentTrackDetailBinding;
import com.manacleindia.iamsafe.db.AppConstants;
import com.manacleindia.iamsafe.db.MVCDBController;
import com.manacleindia.iamsafe.ui.MainActivity;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrackDetailFragment extends Fragment implements DateRVAdapter.SelectionCallback {

    private MVCDBController mvcdbController;
    private FragmentTrackDetailBinding binding;

    private Bundle bundle = new Bundle();
    private TrackerModel trackerModel;
    public TrackDetailFragment() {
        // Required empty public constructor
    }

    public static TrackDetailFragment getInstance() {
        TrackDetailFragment fragment = new TrackDetailFragment();
       /* Bundle bundle = new Bundle();
        bundle.putSerializable("TRACK_MODEL", audioModel);
        fragment.setArguments(bundle);*/
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //bundle = getArguments();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity.getInstance().back.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_track_detail, container, false);
        initRecyclerViews();
        mvcdbController = new MVCDBController(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity.getInstance().back.setVisibility(View.VISIBLE);
        MainActivity.getInstance().refresh.setVisibility(View.GONE);
        binding.rvDateView.setAdapter(new DateRVAdapter(getActivity(), mvcdbController.getAllTrackDateList(), TrackDetailFragment.this));

    }

    private void initRecyclerViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvDateView.setLayoutManager(layoutManager);
        binding.rvDateView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onSelect(int pos, TrackerModel data) {
        TimeDialogFragment noNetworkDialogFragment = new TimeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Date", data.getTrackDate());
        noNetworkDialogFragment.setArguments(bundle);
        noNetworkDialogFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), AppConstants.ERROR_DIALOG_FRAGMENT);

    }
}
