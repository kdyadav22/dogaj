package com.manacleindia.iamsafe.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.databinding.FragmentSettingsBinding;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.pixplicity.easyprefs.library.Prefs;

import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    private FragmentSettingsBinding binding;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment getInstance() {
        /* Bundle bundle = new Bundle();
        bundle.putSerializable("TRACK_MODEL", audioModel);
        fragment.setArguments(bundle);*/
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainActivity.getInstance().back.setVisibility(View.VISIBLE);
        MainActivity.getInstance().refresh.setVisibility(View.GONE);

        binding.wantToVibration.setChecked(Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true));
        binding.wantToSound.setChecked(Prefs.getBoolean(PreferenceConstants.IS_SOUND_CHECKED, true));


        binding.wantToSound.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Prefs.putBoolean(PreferenceConstants.IS_SOUND_CHECKED, isChecked);
        });

        binding.wantToVibration.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Prefs.putBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, isChecked);
        });
    }
}
