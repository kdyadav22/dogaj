package com.manacleindia.iamsafe.ui.dashboard;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayout;
import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.databinding.FragmentDashboardBinding;
import com.manacleindia.iamsafe.ui.MainActivity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class CDashboard extends Fragment {
    private String indiaUrl = "";
    private String worldUrl = "";
    private FragmentDashboardBinding binding;
    private ProgressDialog pDialog;

    public CDashboard() {
        // Required empty public constructor
    }

    public static CDashboard getInstance() {
        CDashboard fragment = new CDashboard();
       /* Bundle bundle = new Bundle();
        bundle.putSerializable("TRACK_MODEL", audioModel);
        fragment.setArguments(bundle);*/
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == 0) {
                    showProgress(getActivity());
                    setHtmlUrl(indiaUrl, binding.myWebview);
                } else {
                    showProgress(getActivity());
                    setHtmlUrl(worldUrl, binding.myWebview);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity.getInstance().back.setVisibility(View.VISIBLE);
        MainActivity.getInstance().refresh.setVisibility(View.GONE);
        showProgress(getActivity());
        setHtmlUrl(indiaUrl, binding.myWebview);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setHtmlUrl(final String htmlLink, final WebView webView) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setAllowContentAccess(true);
            webView.setScrollbarFadingEnabled(false);
            webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);

            webView.setWebViewClient(new WebViewClient() {

                String currentUrl;

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    hideProgress();
                    Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    // Redirect to deprecated method, so you can use it in all SDK versions
                    hideProgress();
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    hideProgress();
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    currentUrl = url;
                    showProgress(getActivity());

                    if (url.startsWith("http") || url.startsWith("https")) {
                        if (url.contains(".pdf")) {
                            String pdfUrl = "https://docs.google.com/gview?embedded=true&url=" + url;
                            webView.loadUrl(pdfUrl);
                            hideProgress();
                            return true;
                        } else
                            return false;
                    }

                    if (url.startsWith("intent")) {
                        try {
                            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            if (fallbackUrl != null) {
                                webView.loadUrl(fallbackUrl);
                                hideProgress();
                                return true;
                            }
                        } catch (URISyntaxException e) {
                            hideProgress();
                            e.getMessage();
                        }
                    } else if (url.startsWith("sms:")) {
                        // Handle the sms: link
                        handleSMSLink(url);
                        hideProgress();
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("whatsapp:")) {
                        // Handle the sms: link
                        try {
                            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            if (fallbackUrl != null) {
                                webView.loadUrl(fallbackUrl);
                                hideProgress();
                                return true;
                            }
                        } catch (URISyntaxException e) {
                            hideProgress();
                            e.getMessage();
                        }

                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("mailto:")) {
                        if (getActivity() != null) {
                            /*MailTo mt = MailTo.parse(url);
                            Intent i = newEmailIntent(getActivity(), mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                            getActivity().startActivity(i);
                            view.reload();*/
                            handleMailToLink(url);
                            hideProgress();
                            return true;
                        }
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("tel:")) {
                        handleTelLink(url);
                        hideProgress();
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    }
                    return true;//do nothing in other cases
                }
            });

            webView.loadUrl(htmlLink);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);

            webView.setOnKeyListener((v, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && webView.canGoBack()) {
                    webView.goBack();
                    setHtmlUrl(htmlLink, webView);
                    hideProgress();
                    return true;
                }

                return false;
            });
        });
    }

    // Custom method to handle web view mailto link
    private void handleMailToLink(String url) {
        // Initialize a new intent which action is send
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        // For only email app handle this intent
        intent.setData(Uri.parse("mailto:"));

        // Extract the email address from mailto url
        String to = url.split("[:?]")[1];
        if (!TextUtils.isEmpty(to)) {
            String[] toArray = to.split(";");
            // Put the primary email addresses array into intent
            intent.putExtra(Intent.EXTRA_EMAIL, toArray);
        }

        // Extract the cc
        if (url.contains("cc=")) {
            String cc = url.split("cc=")[1];
            if (!TextUtils.isEmpty(cc)) {
                //cc = cc.split("&")[0];
                cc = cc.replaceAll("%20", "").split("&")[0];
                String[] ccArray = cc.split(";");
                // Put the cc email addresses array into intent
                intent.putExtra(Intent.EXTRA_CC, ccArray);
            }
        }

        // Extract the bcc
        if (url.contains("bcc=")) {
            String bcc = url.split("bcc=")[1];
            if (!TextUtils.isEmpty(bcc)) {
                //cc = cc.split("&")[0];
                bcc = bcc.replaceAll("%20", "").split("&")[0];
                String[] bccArray = bcc.split(";");
                // Put the bcc email addresses array into intent
                intent.putExtra(Intent.EXTRA_BCC, bccArray);
            }
        }

        // Extract the subject
        if (url.contains("subject=")) {
            String subject = url.split("subject=")[1];
            if (!TextUtils.isEmpty(subject)) {
                subject = subject.split("&")[0];
                // Encode the subject
                try {
                    subject = URLDecoder.decode(subject, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // Put the mail subject into intent
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            }
        }

        // Extract the body
        if (url.contains("body=")) {
            String body = url.split("body=")[1];
            if (!TextUtils.isEmpty(body)) {
                body = body.split("&")[0];
                // Encode the body text
                try {
                    body = URLDecoder.decode(body, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // Put the mail body into intent
                intent.putExtra(Intent.EXTRA_TEXT, body);
            }
        }

        // Email address not null or empty
        if (!TextUtils.isEmpty(to)) {
            if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                // Finally, open the mail client activity
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "No email client found.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    // Custom method to handle sms link
    private void handleSMSLink(String url) {
        /*
            If you want to ensure that your intent is handled only by a text messaging app (and not
            other email or social apps), then use the ACTION_SENDTO action
            and include the "smsto:" data scheme
        */

        // Initialize a new intent to send sms message
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        // Extract the phoneNumber from sms url
        String phoneNumber = url.split("[:?]")[1];

        if (!TextUtils.isEmpty(phoneNumber)) {
            // Set intent data
            // This ensures only SMS apps respond
            intent.setData(Uri.parse("smsto:" + phoneNumber));

            // Alternate data scheme
            //intent.setData(Uri.parse("sms:" + phoneNumber));
        } else {
            // If the sms link built without phone number
            intent.setData(Uri.parse("smsto:"));

            // Alternate data scheme
            //intent.setData(Uri.parse("sms:" + phoneNumber));
        }

        // Extract the sms body from sms url
        if (url.contains("body=")) {
            String smsBody = url.split("body=")[1];

            // Encode the sms body
            try {
                smsBody = URLDecoder.decode(smsBody, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                hideProgress();
                e.printStackTrace();
            }

            if (!TextUtils.isEmpty(smsBody)) {
                // Set intent body
                intent.putExtra("sms_body", smsBody);
            }
        }

        if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
            // Start the sms app
            startActivity(intent);
        } else {
            hideProgress();
            Toast.makeText(getActivity(), "No SMS app found.", Toast.LENGTH_SHORT).show();
        }
    }

    // Custom method to handle html tel: link
    private void handleTelLink(String url) {
        // Initialize an intent to open dialer app with specified phone number
        // It open the dialer app and allow user to call the number manually
        Intent intent = new Intent(Intent.ACTION_DIAL);

        // Send phone number to intent as data
        intent.setData(Uri.parse(url));

        // Start the dialer app activity with number
        startActivity(intent);
    }

    private void showProgress(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait..");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void hideProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
