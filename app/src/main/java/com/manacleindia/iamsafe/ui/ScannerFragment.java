/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.manacleindia.iamsafe.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.manacleindia.iamsafe.ui.dashboard.AdvertiserService;
import com.manacleindia.iamsafe.common.AppController;
import com.manacleindia.iamsafe.ui.dashboard.ForegroundService;
import com.manacleindia.iamsafe.common.FragmentHelper;
import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.ui.detail.TrackerModel;
import com.manacleindia.iamsafe.common.Utility;
import com.manacleindia.iamsafe.databinding.FragmentBlankBinding;
import com.manacleindia.iamsafe.db.MVCDBController;
import com.manacleindia.iamsafe.ui.dashboard.CDashboard;
import com.manacleindia.iamsafe.ui.detail.TrackDetailFragment;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;


/**
 * Scans for Bluetooth Low Energy Advertisements matching a filter and displays them to the user.
 */
public class ScannerFragment extends Fragment {
    private static final String TAG = ScannerFragment.class.getSimpleName();

    @SuppressLint("SimpleDateFormat")
    private
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    @SuppressLint("SimpleDateFormat")
    private
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

    /**
     * Stops scanning after 5 seconds.
     */
    private static final long SCAN_PERIOD = 1000 * 60 * 60 * 24;

    private BluetoothAdapter mBluetoothAdapter;

    private BluetoothLeScanner mBluetoothLeScanner;

    private ScanCallback mScanCallback;

    private Handler mHandler;

    private ProgressDialog pDialog;

    private MVCDBController mvcdbController;

    private String url = "";

    /*BLE Advertisement*/
    /**
     * Listens for notifications that the {@code AdvertiserService} has failed to start advertising.
     * This Receiver deals with Fragment UI elements and only needs to be active when the Fragment
     * is on-screen, so it's defined and registered in code instead of the Manifest.
     */
    private BroadcastReceiver advertisingFailureReceiver;
    /*END*/

    /**
     * Must be called after object creation by MainActivity.
     *
     * @param btAdapter the local BluetoothAdapter
     */
    void setBluetoothAdapter(BluetoothAdapter btAdapter) {
        this.mBluetoothAdapter = btAdapter;
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
    }

    private FragmentBlankBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        mHandler = new Handler();
        mvcdbController = new MVCDBController(getActivity());
        advertisingFailureReceiver = new BroadcastReceiver() {

            /**
             * Receives Advertising error codes from {@code AdvertiserService} and displays error messages
             * to the user. Sets the advertising toggle to 'false.'
             */
            @Override
            public void onReceive(Context context, Intent intent) {

                int errorCode = intent.getIntExtra(AdvertiserService.ADVERTISING_FAILED_EXTRA_CODE, -1);

                String errorMessage = getString(R.string.start_error_prefix);
                switch (errorCode) {
                    case AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED:
                        errorMessage += " " + getString(R.string.start_error_already_started);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE:
                        errorMessage += " " + getString(R.string.start_error_too_large);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                        errorMessage += " " + getString(R.string.start_error_unsupported);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR:
                        errorMessage += " " + getString(R.string.start_error_internal);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                        errorMessage += " " + getString(R.string.start_error_too_many);
                        break;
                    case AdvertiserService.ADVERTISING_TIMED_OUT:
                        errorMessage = " " + getString(R.string.advertising_timedout);
                        startAdvertising();
                        break;
                    default:
                        errorMessage += " " + getString(R.string.start_error_unknown);
                }

                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
            }
        };

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_blank, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Trigger refresh on app's 1st load
        startScanning();

        startAdvertising();

        try {
            MainActivity.getInstance().refresh.setOnClickListener(v -> {
               /* startAdvertising();
                startScanning();*/

                Toast.makeText(getActivity(), "Hiii", Toast.LENGTH_SHORT).show();
            });
        } catch (Exception e) {
            e.getMessage();
        }

        binding.physicalDistanceHistory.setOnClickListener(v -> loadTrackDetailFragment());

        binding.dashboard.setOnClickListener(v -> Toast.makeText(getActivity(), "Coming soon...", Toast.LENGTH_SHORT).show());

        /*if (mvcdbController.isAnyTrackExist())
            count.setText(String.format("Alert Count : %d", mvcdbController.getTotalCount()));*/

    }

    /**
     * Start scanning for BLE Advertisements (& set it up to stop after a set period of time).
     */
    private void startScanning() {
        if (mScanCallback == null) {
            Log.d(TAG, "Starting Scanning");

            // Will stop the scanning after a set time.
            mHandler.postDelayed(this::stopScanning, SCAN_PERIOD);

            // Kick off a new scan.
            mScanCallback = new SampleScanCallback();
            mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);

            String toastText = getString(R.string.scan_start_toast) + " "
                    + TimeUnit.SECONDS.convert(SCAN_PERIOD, TimeUnit.MILLISECONDS) + " "
                    + getString(R.string.seconds);
            //Toast.makeText(getActivity(), toastText, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), R.string.already_scanning, Toast.LENGTH_SHORT);
        }
    }

    /**
     * Stop scanning for BLE Advertisements.
     */
    private void stopScanning() {
        Log.d(TAG, "Stopping Scanning");

        // Stop the scan, wipe the callback.
        mBluetoothLeScanner.stopScan(mScanCallback);
        mScanCallback = null;

        // Even if no new results, update 'last seen' times.
        //mAdapter.notifyDataSetChanged();
    }

    /**
     * Return a List of {@link ScanFilter} objects to filter by Service UUID.
     */
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        // Comment out the below line to see all BLE devices around you
        builder.setServiceUuid(Constants.Service_UUID);
        scanFilters.add(builder.build());

        return scanFilters;
    }

    /**
     * Return a {@link ScanSettings} object set to use low power (to preserve battery life).
     */
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        return builder.build();
    }

    /**
     * Custom ScanCallback object - adds to adapter on success, displays error on failure.
     */
    private class SampleScanCallback extends ScanCallback {

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);

            /*for (ScanResult result : results) {
                mAdapter.add(result);
            }
            mAdapter.notifyDataSetChanged();*/
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            if (result != null) {
               /* mAdapter.add(result);
                mAdapter.notifyDataSetChanged();*/

                DecimalFormat form = new DecimalFormat("0.00");
                form.setRoundingMode(RoundingMode.CEILING);
                double dis3 = getDistance(result.getRssi(), -69);
                if (dis3 >= 3) {
                    Log.v("TAG", "GREEN");
                } else if (dis3 <= 1) {
                    Log.v("TAG", "RED");
                    TrackerModel trackerModel = new TrackerModel();

                    trackerModel.setTrackId(Utility.unique_ProfileId());

                    String name = result.getDevice().getName();
                    if (name == null) {
                        name = Objects.requireNonNull(getActivity()).getResources().getString(R.string.no_name);
                    }

                    trackerModel.setBleDeviceName(name);
                    trackerModel.setBleMacAddress(result.getDevice().getAddress());
                    trackerModel.setDeviceName("Android");
                    trackerModel.setDeviceToken(String.valueOf(System.currentTimeMillis()));
                    trackerModel.setImeiNumber("");

                    try {
                        trackerModel.setIpAddress("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    trackerModel.setLatitude("1");
                    trackerModel.setLongitude("1");
                    trackerModel.setOsVersion(getOsVersion());

                    trackerModel.setTrackDate(Utility.formattedDateString(dateFormat, new Date()));

                    DateFormat formatter = new SimpleDateFormat("hh:mm a");
                    //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                    String text = formatter.format(new Date(System.currentTimeMillis()));

                    trackerModel.setTrackTime(text);

                    if (mvcdbController.addTrackData(trackerModel) > 0) {
                        sendNotification("You are in the Risk! You must have to follow Social Distancing", "Red Zone", 1);
                    }

                    //count.setText(String.format("Alert Count : %d", mvcdbController.getTotalCount()));

                } else if (dis3 > 1 && dis3 < 3) {
                    Log.v("TAG", "ORANGE");
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Toast.makeText(getActivity(), "Scan failed with error: " + errorCode, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startScanning();

        if (!AdvertiserService.running) {
            startAdvertising();
        }

        IntentFilter failureFilter = new IntentFilter(AdvertiserService.ADVERTISING_FAILED);
        Objects.requireNonNull(getActivity()).registerReceiver(advertisingFailureReceiver, failureFilter);
    }

    /**
     * Starts BLE Advertising by starting {@code AdvertiserService}.
     */
    private void startAdvertising() {
        Context c = getActivity();
        Objects.requireNonNull(c).startService(getServiceIntent(c));
    }

    /**
     * Stops BLE Advertising by stopping {@code AdvertiserService}.
     */
    private void stopAdvertising() {
        Context c = getActivity();
        Objects.requireNonNull(c).stopService(getServiceIntent(c));
    }

    /**
     * Returns Intent addressed to the {@code AdvertiserService} class.
     */
    private static Intent getServiceIntent(Context c) {
        return new Intent(c, AdvertiserService.class);
    }

    private void sendNotification(String messageBody, String title, int zoneType) {
        try {
            Random r = new Random();
            final int NOTIFICATION_ID = 101;//r.nextInt(10000);

            if (getActivity() == null) {
                startService(messageBody, title);
                return;
            }


            Intent intent = new Intent(getActivity(), MainActivity.class);

            intent.putExtra("isComingFromNotification", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            String channelId = Objects.requireNonNull(getActivity()).getString(R.string.default_notification_channel_id);
            //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Uri defaultSoundUri = Uri.parse("android.resource://" + AppController.getInstance().getPackageName() + "/" + R.raw.siren_noise);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getActivity(), channelId);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setContentTitle(title);
            notificationBuilder.setContentText(messageBody);
            //.setContent(contentView)

            if (Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true)) {
                notificationBuilder.setVibrate(new long[]{1000, 1000});
                notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);

            }

            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setWhen(System.currentTimeMillis());

            if (Prefs.getBoolean(PreferenceConstants.IS_SOUND_CHECKED, true)) {
                notificationBuilder.setSound(defaultSoundUri);
            }
            notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
            notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    AudioAttributes.Builder attrs = new AudioAttributes.Builder();
                    attrs.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC);
                    attrs.setUsage(AudioAttributes.USAGE_MEDIA);
                    //Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel notificationChannel = new NotificationChannel(channelId, "AmSafe", importance);
                    notificationChannel.enableLights(true);
                    notificationChannel.setImportance(importance);
                    notificationChannel.setLightColor(Color.BLUE);

                    if (Prefs.getBoolean(PreferenceConstants.IS_VIBRATION_CHECKED, true)) {
                        notificationChannel.enableVibration(true);
                        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    }

                    if (Prefs.getBoolean(PreferenceConstants.IS_SOUND_CHECKED, true)) {
                        notificationChannel.setSound(defaultSoundUri, attrs.build());
                    }

                    //notificationChannel.setShowBadge(true);
                    notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
                    notificationBuilder.setChannelId(channelId);
                    Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            Objects.requireNonNull(notificationManager).notify(NOTIFICATION_ID, notificationBuilder.build());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private double getDistance(int rssi, int txPower) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         *
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */

        return Math.pow(10d, ((double) txPower - rssi) / (10 * 2));
    }

    private void startService(String messageBody, String title) {
        Intent serviceIntent = new Intent(AppController.getInstance(), ForegroundService.class);
        serviceIntent.putExtra("inputTitle", title);
        serviceIntent.putExtra("inputMessage", messageBody);
        ContextCompat.startForegroundService(Objects.requireNonNull(AppController.getInstance()), serviceIntent);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setHtmlUrl(final String htmlLink, final WebView webView) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setAllowContentAccess(true);
            webView.setScrollbarFadingEnabled(false);
            webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);

            webView.setWebViewClient(new WebViewClient() {

                String currentUrl;

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    hideProgress();
                    Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                @Override
                public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                    // Redirect to deprecated method, so you can use it in all SDK versions
                    hideProgress();
                    onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    hideProgress();
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    currentUrl = url;
                    showProgress(getActivity());

                    if (url.startsWith("http") || url.startsWith("https")) {
                        if (url.contains(".pdf")) {
                            String pdfUrl = "https://docs.google.com/gview?embedded=true&url=" + url;
                            webView.loadUrl(pdfUrl);
                            hideProgress();
                            return true;
                        } else
                            return false;
                    }

                    if (url.startsWith("intent")) {
                        try {
                            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            if (fallbackUrl != null) {
                                webView.loadUrl(fallbackUrl);
                                hideProgress();
                                return true;
                            }
                        } catch (URISyntaxException e) {
                            hideProgress();
                            e.getMessage();
                        }
                    } else if (url.startsWith("sms:")) {
                        // Handle the sms: link
                        handleSMSLink(url);
                        hideProgress();
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("whatsapp:")) {
                        // Handle the sms: link
                        try {
                            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                            String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                            if (fallbackUrl != null) {
                                webView.loadUrl(fallbackUrl);
                                hideProgress();
                                return true;
                            }
                        } catch (URISyntaxException e) {
                            hideProgress();
                            e.getMessage();
                        }

                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("mailto:")) {
                        if (getActivity() != null) {
                            /*MailTo mt = MailTo.parse(url);
                            Intent i = newEmailIntent(getActivity(), mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                            getActivity().startActivity(i);
                            view.reload();*/
                            handleMailToLink(url);
                            hideProgress();
                            return true;
                        }
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    } else if (url.startsWith("tel:")) {
                        handleTelLink(url);
                        hideProgress();
                        // Return true means, leave the current web view and handle the url itself
                        return true;
                    }
                    return true;//do nothing in other cases
                }
            });

            webView.loadUrl(htmlLink);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setJavaScriptEnabled(true);

            webView.setOnKeyListener((v, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == MotionEvent.ACTION_UP
                        && webView.canGoBack()) {
                    webView.goBack();
                    setHtmlUrl(url, webView);
                    hideProgress();
                    return true;
                }

                return false;
            });
        });
    }

    // Custom method to handle web view mailto link
    private void handleMailToLink(String url) {
        // Initialize a new intent which action is send
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        // For only email app handle this intent
        intent.setData(Uri.parse("mailto:"));

        // Extract the email address from mailto url
        String to = url.split("[:?]")[1];
        if (!TextUtils.isEmpty(to)) {
            String[] toArray = to.split(";");
            // Put the primary email addresses array into intent
            intent.putExtra(Intent.EXTRA_EMAIL, toArray);
        }

        // Extract the cc
        if (url.contains("cc=")) {
            String cc = url.split("cc=")[1];
            if (!TextUtils.isEmpty(cc)) {
                //cc = cc.split("&")[0];
                cc = cc.replaceAll("%20", "").split("&")[0];
                String[] ccArray = cc.split(";");
                // Put the cc email addresses array into intent
                intent.putExtra(Intent.EXTRA_CC, ccArray);
            }
        }

        // Extract the bcc
        if (url.contains("bcc=")) {
            String bcc = url.split("bcc=")[1];
            if (!TextUtils.isEmpty(bcc)) {
                //cc = cc.split("&")[0];
                bcc = bcc.replaceAll("%20", "").split("&")[0];
                String[] bccArray = bcc.split(";");
                // Put the bcc email addresses array into intent
                intent.putExtra(Intent.EXTRA_BCC, bccArray);
            }
        }

        // Extract the subject
        if (url.contains("subject=")) {
            String subject = url.split("subject=")[1];
            if (!TextUtils.isEmpty(subject)) {
                subject = subject.split("&")[0];
                // Encode the subject
                try {
                    subject = URLDecoder.decode(subject, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // Put the mail subject into intent
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            }
        }

        // Extract the body
        if (url.contains("body=")) {
            String body = url.split("body=")[1];
            if (!TextUtils.isEmpty(body)) {
                body = body.split("&")[0];
                // Encode the body text
                try {
                    body = URLDecoder.decode(body, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // Put the mail body into intent
                intent.putExtra(Intent.EXTRA_TEXT, body);
            }
        }

        // Email address not null or empty
        if (!TextUtils.isEmpty(to)) {
            if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
                // Finally, open the mail client activity
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "No email client found.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    // Custom method to handle sms link
    private void handleSMSLink(String url) {
        /*
            If you want to ensure that your intent is handled only by a text messaging app (and not
            other email or social apps), then use the ACTION_SENDTO action
            and include the "smsto:" data scheme
        */

        // Initialize a new intent to send sms message
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        // Extract the phoneNumber from sms url
        String phoneNumber = url.split("[:?]")[1];

        if (!TextUtils.isEmpty(phoneNumber)) {
            // Set intent data
            // This ensures only SMS apps respond
            intent.setData(Uri.parse("smsto:" + phoneNumber));

            // Alternate data scheme
            //intent.setData(Uri.parse("sms:" + phoneNumber));
        } else {
            // If the sms link built without phone number
            intent.setData(Uri.parse("smsto:"));

            // Alternate data scheme
            //intent.setData(Uri.parse("sms:" + phoneNumber));
        }

        // Extract the sms body from sms url
        if (url.contains("body=")) {
            String smsBody = url.split("body=")[1];

            // Encode the sms body
            try {
                smsBody = URLDecoder.decode(smsBody, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                hideProgress();
                e.printStackTrace();
            }

            if (!TextUtils.isEmpty(smsBody)) {
                // Set intent body
                intent.putExtra("sms_body", smsBody);
            }
        }

        if (intent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
            // Start the sms app
            startActivity(intent);
        } else {
            hideProgress();
            Toast.makeText(getActivity(), "No SMS app found.", Toast.LENGTH_SHORT).show();
        }
    }

    // Custom method to handle html tel: link
    private void handleTelLink(String url) {
        // Initialize an intent to open dialer app with specified phone number
        // It open the dialer app and allow user to call the number manually
        Intent intent = new Intent(Intent.ACTION_DIAL);

        // Send phone number to intent as data
        intent.setData(Uri.parse(url));

        // Start the dialer app activity with number
        startActivity(intent);
    }

    private void showProgress(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait..");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void hideProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    private String getOsVersion() {
        String myVersion = android.os.Build.VERSION.RELEASE; // e.g. myVersion := "1.6"
        int sdkVersion = android.os.Build.VERSION.SDK_INT;
        return myVersion;
    }

    private void loadTrackDetailFragment() {
        try {
            FragmentHelper.replaceFragmentAddToBackstack(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), MainActivity.getInstance().getContainerId(),
                    TrackDetailFragment.getInstance(), TrackDetailFragment.class.getName());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void loaddashboardFragment() {
        try {
            FragmentHelper.replaceFragmentAddToBackstack(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), MainActivity.getInstance().getContainerId(),
                    CDashboard.getInstance(), CDashboard.class.getName());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void loadSettingsFragment() {
        try {
            FragmentHelper.replaceFragmentAddToBackstack(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), MainActivity.getInstance().getContainerId(),
                    SettingsFragment.getInstance(), SettingsFragment.class.getName());
        } catch (Exception e) {
            e.getMessage();
        }
    }

}
