package com.manacleindia.iamsafe.ui.detail;

import android.app.Dialog;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.db.MVCDBController;

import java.util.List;
import java.util.Objects;

/**
 * Created by Kapil on 30-01-2019.
 */

public class TimeDialogFragment extends DialogFragment{
    private String date;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        date = (String) Objects.requireNonNull(getArguments()).get("Date");
        setStyle(DialogFragment.STYLE_NORMAL,
                R.style.DialogFragmentTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialog.getWindow()).setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        //Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        com.manacleindia.iamsafe.databinding.TimeLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.time_layout, container, false);
        MVCDBController mvcdbController = new MVCDBController(getActivity());
        binding.closeDialog.setOnClickListener(v -> dismiss());
        setTag(mvcdbController.getAllTrackTimeList(date), binding.tagGroup);
        return binding.getRoot();
    }

    private void setTag(final List<TrackerModel> tagList, ChipGroup chipGroup) {
        for (int index = 0; index < tagList.size(); index++) {
            final TrackerModel tagName = tagList.get(index);
            final Chip chip = new Chip(Objects.requireNonNull(getActivity()));
            int paddingDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
            chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp);
            chip.setText(tagName.getTrackTime());
            chip.setTextAppearance(R.style.chipTextAppearance);
            //chip.setCloseIconResource(R.drawable.ic_action_navigation_close);
            //chip.setCloseIconEnabled(true);
            //Added click listener on close icon to remove tag from ChipGroup
            chip.setOnClickListener(v -> {
                //tagList.remove(tagName);
                //chipGroup.removeView(chip);
                //Toast.makeText(getActivity(), "Hii", Toast.LENGTH_LONG).show();
            });

            chipGroup.addView(chip);
        }
    }
}