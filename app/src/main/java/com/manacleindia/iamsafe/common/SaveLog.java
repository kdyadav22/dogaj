package com.manacleindia.iamsafe.common;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SaveLog {

    public static void saveLog(String title, String message) {
        String TAG = "Logs";
        String PRIVATE_FILE = "iamsafe.txt";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String timeText = formatter.format(new Date(System.currentTimeMillis()));
        try {
            FileWriter fileWriter;

            File dir = Environment.getExternalStorageDirectory();
            if (!dir.exists()) {
                return;
            } else {
                dir = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath());
                if (!dir.exists()) {
                    dir.mkdirs();
                    dir.setReadable(true, true);
                    dir.setWritable(true, true);

                    Log.d(TAG, "d: created new directory by mkdir(): " + dir.getAbsolutePath());
                }

                File file = new File(dir.getAbsolutePath() + "/" + PRIVATE_FILE);
                // true = append file
                fileWriter = new FileWriter(file, true);
            }


            PrintWriter pw = new PrintWriter(new BufferedWriter(fileWriter), true);

            pw.println("Time"+timeText+" "+title + ":");
            pw.println(message);
            pw.println("");

            pw.flush();
            pw.close();
            fileWriter.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "error, file " + PRIVATE_FILE + " cannot be open!");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "error, file " + PRIVATE_FILE + " write failed!");
            e.printStackTrace();
        }
    }

    public static void deleteLogFile() {
        File dir = Environment.getExternalStorageDirectory();
        if (dir.exists()) {
            dir = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/iamsafe.txt");
            if (dir.exists()) {
                dir.delete();
            }
        }
    }
}