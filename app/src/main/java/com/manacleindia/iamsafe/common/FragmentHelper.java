

package com.manacleindia.iamsafe.common;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.lang.ref.WeakReference;

public class FragmentHelper {

    /**
     * This method pops all the fragments which matches with the specified tag from back-stack and
     * replaces the given fragment.
     *
     * @param manager   Fragment Manager
     * @param container container view Id
     * @param fragment  Fragment
     * @param tag       optional tag name for the fragment
     */
    public static void popBackStackAndReplace(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        manager.beginTransaction().replace(container, fragment, tag).commit();

    }

    /**
     * Replace the fragment
     *
     * @param manager   Calling FragmentManager instance
     * @param container Container in which we are replacing fragment
     * @param fragment  Instance of fragment want to replace
     */
    public static void replaceFragment(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.beginTransaction().replace(container, fragment, tag).commitAllowingStateLoss();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(fragment));
    }

    /**
     * Add the fragment
     *
     * @param manager   Calling FragmentManager instance
     * @param container Container in which we are replacing fragment
     * @param fragment  Instance of fragment want to add
     */
    public static void addFragment(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.beginTransaction().add(container, fragment, tag).commitAllowingStateLoss();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(fragment));
    }

    /**
     * Removes the fragment
     *
     * @param fragment Instance of fragment want to remove
     */
    public static void removeFragment(FragmentManager manager, Fragment fragment) {
        manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        ScreenTagManager.fragmentStack.remove(fragment);
    }

    /**
     * Add the fragment
     *
     * @param manager   Calling FragmentManager instance
     * @param container Container in which we are replacing fragment
     * @param fragment  Instance of fragment want to add
     */
    public static void addFragment(FragmentManager manager, int container, Fragment fragment, int enterAnimation, int exitAnimation, String tag) {
        manager.beginTransaction().setCustomAnimations(enterAnimation, exitAnimation, enterAnimation, exitAnimation).add(container, fragment, tag).commitAllowingStateLoss();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(fragment));
    }

    public static void replaceFragmentAddToBackstack(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.beginTransaction().addToBackStack(tag).replace(
                container, fragment, tag).commitAllowingStateLoss();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(fragment));
    }

    public static void replaceFragmentNoBackstack(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.beginTransaction().addToBackStack(null).replace(
                container, fragment, tag).commit();
        ScreenTagManager.fragmentStack.remove(new WeakReference<>(fragment));
    }

    public static void replaceFragmentAddToBackstackNoLoss(FragmentManager manager, int container, Fragment fragment, String tag) {
        manager.beginTransaction().addToBackStack(tag).replace(
                container, fragment, tag).commit();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(fragment));
    }


    /**
     * Add the fragment and add it to local stack for stacking logic
     *
     * @param fm          Calling FragmentManager instance
     * @param container   Container in which we are replacing fragment
     * @param newFragment Instance of fragment want to add
     */
    public static void addFragmentToBackStack(FragmentManager fm, int container, Fragment newFragment,
                                              String tag) {
        fm.beginTransaction().add(container, newFragment, tag).addToBackStack(tag).commitAllowingStateLoss();
        //fm.executePendingTransactions();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(newFragment));
    }

    public static void addFragmentToBackStackNoLoss(FragmentManager fm, int container, Fragment newFragment,
                                                    String tag) {
        fm.beginTransaction().add(container, newFragment, tag).addToBackStack(tag).commit();
        //fm.executePendingTransactions();
        ScreenTagManager.fragmentStack.add(new WeakReference<>(newFragment));
    }

    public static void popBackStackImmediate(FragmentActivity activity) {
        if (activity != null && !activity.isFinishing())
            activity.getSupportFragmentManager().popBackStackImmediate();
    }
}
