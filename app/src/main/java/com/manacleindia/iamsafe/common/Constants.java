package com.manacleindia.iamsafe.common;

import android.os.ParcelUuid;

/**
 * Constants for use in the Bluetooth Advertisements sample
 */
public class Constants {

    /**
     * UUID identified with this app - set as Service UUID for BLE Advertisements.
     */
    public static final ParcelUuid Service_UUID = ParcelUuid.fromString("eb8b4426-9b92-4b9f-9302-a123f5f4d6d4");

    public static final int REQUEST_ENABLE_BT = 1;

    public static final String
            ACTION_STATE_CHANGED = "android.bluetooth.adapter.action.STATE_CHANGED";
    public static final String
            AIRPLANE_STATE_CHANGED = "android.intent.action.AIRPLANE_MODE";
    public static final String
            NETWORK_STATE_CHANGED = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String
            LOCATION_STATE_CHANGED = "android.location.PROVIDERS_CHANGED";
    public static final String
            BOOT_COMPLETED_STATE_CHANGED = "android.permission.RECEIVE_BOOT_COMPLETED";

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    // FIXME: 9/26/19
    public static final long UPDATE_INTERVAL = 50 * 1000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    // FIXME: 9/26/19
    public static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2;

    /**
     * The max time before batched results are delivered by location services. Results may be
     * delivered sooner than this interval.
     */
    public static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 2;

}
