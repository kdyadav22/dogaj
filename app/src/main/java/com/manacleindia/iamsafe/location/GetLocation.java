package com.manacleindia.iamsafe.location;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.manacleindia.iamsafe.common.AppController;
import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.common.SaveLog;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.pixplicity.easyprefs.library.Prefs;

import static android.content.Context.LOCATION_SERVICE;

public class GetLocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "LocationTracking";
    private AppCompatActivity activity;
    private Context context;
    private static GetLocation locationInstance;

    public GetLocation(AppCompatActivity activity) {
        this.activity = activity;
        context = activity;
        buildGoogleApiClient();
    }

    public GetLocation(Context context) {
        this.context = context;
        buildGoogleApiClient1();
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .enableAutoManage(activity, this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    private void buildGoogleApiClient1() {
        if (mGoogleApiClient != null) {
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    public static synchronized GetLocation getInstance() {
        return locationInstance;
    }

    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(Constants.UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(Constants.FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(Constants.MAX_WAIT_TIME);
        locationInstance = this;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        final String text = "Connection suspended";
        Log.w(TAG, text + ": Error code: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        final String text = "Exception while connecting to Google Play services";
        Log.w(TAG, text + ": " + connectionResult.getErrorMessage());
    }

    public void getLastLocationUpdates() {
        try {
            Log.i(TAG, "Starting location updates");
            LocationServices.getFusedLocationProviderClient(context).getLastLocation().addOnSuccessListener(location -> {
                Prefs.putDouble(PreferenceConstants.LOCATION_LAT, location.getLatitude());
                Prefs.putDouble(PreferenceConstants.LOCATION_LNG, location.getLongitude());

                Log.v("LATITUDE ", "From Location "+location.getLatitude());
                Log.v("LONGITUDE ", "From Location "+location.getLongitude());

            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the Request Updates button and requests start of location updates.
     */
    public void getContinueLocationUpdates() {
        try {
            Log.i(TAG, "Starting location updates");
            Prefs.putBoolean(PreferenceConstants.LOCATION_UPDATE, true);
            LocationServices.getFusedLocationProviderClient(context).requestLocationUpdates(mLocationRequest, getPendingIntent());
        } catch (SecurityException e) {
            Prefs.putBoolean(PreferenceConstants.LOCATION_UPDATE, false);
            SaveLog.saveLog("SecurityException: ", e.getMessage());
        } catch (Exception e) {
            Prefs.putBoolean(PreferenceConstants.LOCATION_UPDATE, false);
            SaveLog.saveLog("Exception: ", e.getMessage());
        }
    }

    /**
     * Handles the Remove Updates button, and requests removal of location updates.
     */
    public void removeLocationUpdates(AppController appController) {
        Log.i(TAG, "Removing location updates");
        Prefs.putBoolean(PreferenceConstants.LOCATION_UPDATE, false);
        LocationServices.getFusedLocationProviderClient(context).removeLocationUpdates(getPendingIntent());
    }

    private PendingIntent getPendingIntent() {
        Intent intent = new Intent(context, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public boolean isGPSEnable() {
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (locationManager != null)
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        else return false;
    }
}
