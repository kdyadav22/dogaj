package com.manacleindia.iamsafe.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.manacleindia.iamsafe.common.SaveLog;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Objects;

public class GpsLocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.requireNonNull(intent.getAction()).matches("android.location.PROVIDERS_CHANGED")) {
            if(Prefs.getBoolean(PreferenceConstants.LOCATION_UPDATE, false)) {
                if (GetLocation.getInstance() != null && GetLocation.getInstance().isGPSEnable()) {
                    SaveLog.saveLog("GpsLocationReceiver", "GPS Enable");
                } else {
                    SaveLog.saveLog("GpsLocationReceiver", "GPS Disabled");
                }
            }
        }
    }
}
