package com.manacleindia.iamsafe.location;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.manacleindia.iamsafe.common.Constants;
import com.manacleindia.iamsafe.receiver.MyLocationProvideChangeBroadcastReceiver;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.pixplicity.easyprefs.library.Prefs;

import static com.manacleindia.iamsafe.common.SaveLog.saveLog;

public class StartUpService extends JobIntentService {

    public static final int JOB_ID = 0x04;
    MyLocationProvideChangeBroadcastReceiver myLocationProvideChangeBroadcastReceiver;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, StartUpService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // your code
        saveLog("BootComplete StartUp Service", "BootComplete StartUp Service ");
        try {
            if (GetLocation.getInstance() != null && GetLocation.getInstance().isGPSEnable()) {
                if (Prefs.getBoolean(PreferenceConstants.LOCATION_UPDATE, false)) {
                    GetLocation getLocation = new GetLocation(this);
                    getLocation.createLocationRequest();
                    getLocation.getContinueLocationUpdates();
                }
            }
        } catch (Exception e) {
            saveLog("Exception: ", "Services " + e.getMessage());
        }

        try {
            myLocationProvideChangeBroadcastReceiver = new MyLocationProvideChangeBroadcastReceiver();
            registerReceiver(myLocationProvideChangeBroadcastReceiver, new IntentFilter(Constants.LOCATION_STATE_CHANGED));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(myLocationProvideChangeBroadcastReceiver);
        } catch (Exception e) {
            e.getMessage();
        }
    }
}