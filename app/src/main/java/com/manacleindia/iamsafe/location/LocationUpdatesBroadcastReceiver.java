package com.manacleindia.iamsafe.location;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.android.gms.location.LocationResult;
import com.manacleindia.iamsafe.R;
import com.manacleindia.iamsafe.common.AppController;
import com.manacleindia.iamsafe.common.SaveLog;
import com.manacleindia.iamsafe.services.PreferenceConstants;
import com.manacleindia.iamsafe.ui.Splash;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.List;

import static android.content.Context.POWER_SERVICE;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_DISABLED;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_ENABLED;
import static android.net.ConnectivityManager.RESTRICT_BACKGROUND_STATUS_WHITELISTED;

/**
 * Receiver for handling location updates.
 * <p>
 * For apps targeting API level O
 * {@link PendingIntent#getBroadcast(Context, int, Intent, int)} should be used when
 * requesting location updates. Due to limits on background services,
 * {@link PendingIntent#getService(Context, int, Intent, int)} should not be used.
 * <p>
 * Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 * less frequently than the interval specified in the
 * {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 * foreground.
 */
public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private AppController appController = AppController.getInstance();
    static final String ACTION_PROCESS_UPDATES = "ACTION_LOCATION_PROCESS_UPDATES";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            try {
                final String action = intent.getAction();
                if (ACTION_PROCESS_UPDATES.equals(action)) {
                    LocationResult result = LocationResult.extractResult(intent);
                    if (result != null) {
                        List<Location> locations = result.getLocations();
                        Log.v("KEEP DISTANCE LOCATION ", "From BR - Latitude: " + locations.get(0).getLatitude() + " Longitude: " + locations.get(0).getLongitude());

                        Prefs.putDouble(PreferenceConstants.LOCATION_LAT, locations.get(0).getLatitude());
                        Prefs.putDouble(PreferenceConstants.LOCATION_LNG, locations.get(0).getLongitude());
                    }
                } else {
                    SaveLog.saveLog("Location Receiver Action:", action);
                }
            } catch (Exception e) {
                SaveLog.saveLog("Location Receiver Exception:", e.getMessage());
            }
        }
    }

    private void wakeUpDevice(Context context) {
        try {
            PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
            assert pm != null;
            boolean isScreenOn = pm.isScreenOn();
            //Log.e("screen on.................................", ""+isScreenOn);
            if (!isScreenOn) {
                PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "myApp:notificationLock");
                wl.acquire(20000);
                PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myApp:MyCpuLock");
                wl_cpu.acquire(20000);
            }
        } catch (Exception e) {
            SaveLog.saveLog("Location Receiver Wake lock Exception:", e.getMessage());
        }
    }

    private void showNotification(Context context) {
        Intent notificationIntent = new Intent(context, Splash.class);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(Splash.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= 26) {
            String PRIMARY_CHANNEL = "default";
            NotificationChannel channel = new NotificationChannel(PRIMARY_CHANNEL,
                    context.getString(R.string.default_notification_channel_id), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setLightColor(Color.GREEN);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            if (mNotificationManager != null)
                mNotificationManager.createNotificationChannel(channel);

            Notification.Builder notificationBuilder = new Notification.Builder(context, PRIMARY_CHANNEL)
                    .setContentTitle("Gps Searching...")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(false)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setContentIntent(notificationPendingIntent);
            if (mNotificationManager != null)
                mNotificationManager.notify(0, notificationBuilder.build());
        } else {
            Notification.Builder notificationBuilder = new Notification.Builder(context)
                    .setContentTitle("Gps Searching...")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(false)
                    .setContentIntent(notificationPendingIntent);
            if (mNotificationManager != null)
                mNotificationManager.notify(0, notificationBuilder.build());
        }
    }

    private void checkConnectivity(Context context) {
        try {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr != null ? connMgr.getActiveNetworkInfo() : null;

            if (networkInfo == null) {
                SaveLog.saveLog("Location Receiver Exception:", "Continue internet disconnected due to null");
                return;
            }

            if (networkInfo.isConnected()) {
                // Checks if the device is on a metered network
                if (Build.VERSION.SDK_INT >= 24) {
                    if (connMgr.isActiveNetworkMetered()) {
                        // Checks user’s Data Saver settings.
                        switch (connMgr.getRestrictBackgroundStatus()) {
                            case RESTRICT_BACKGROUND_STATUS_ENABLED:
                                // Background data usage is blocked for this app. Wherever possible,
                                // the app should also use less data in the foreground.
                                SaveLog.saveLog("RESTRICT_BACKGROUND_STATUS_ENABLED", "Background data usage is blocked for this app. Wherever possible.");
                            case RESTRICT_BACKGROUND_STATUS_WHITELISTED:
                                // The app is whitelisted. Wherever possible,
                                // the app should use less data in the foreground and background.
                                SaveLog.saveLog("RESTRICT_BACKGROUND_STATUS_WHITELISTED", "The app is whitelisted. Wherever possible");

                            case RESTRICT_BACKGROUND_STATUS_DISABLED:
                                // Data Saver is disabled. Since the device is connected to a
                                // metered network, the app should use less data wherever possible.
                                SaveLog.saveLog("RESTRICT_BACKGROUND_STATUS_DISABLED", "Data Saver is disabled. Since the device is connected to a");
                        }
                    } else {
                        // The device is not on a metered network.
                        // Use data as required to perform syncs, downloads, and updates.
                        SaveLog.saveLog("RESTRICT_BACKGROUND_STATUS_DISABLED", " The device is not on a metered network.");
                    }
                } else {
                    SaveLog.saveLog("Location Update BR", " ");
                }
            } else {
                SaveLog.saveLog("Location Update BR", "Continue internet disconnected. ");
            }
        } catch (Exception e) {
            SaveLog.saveLog("Location Update BR", "Exception: " + e.getMessage());
        }
    }

    /*private void updateToServer(Context context) {
        try{
            wakeUpDevice(context);
            updateSingleTravelLocationDataToServer(context);
            new UpdateUserLogsToServer().updateLogToServer(context);
        }catch (Exception e){
            new LogUserActivity().updateUserActivity(context, "Update To Server Exception: "+e.getMessage());
        }
    }*/
}