package com.manacleindia.iamsafe.location;

import android.location.Location;

public interface IUpdateLocationListener {
    void onUpdateLocation(Location location, String action);
}